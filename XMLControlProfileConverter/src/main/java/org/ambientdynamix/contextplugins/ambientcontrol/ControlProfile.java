package org.ambientdynamix.contextplugins.ambientcontrol;

import java.util.*;

/**
 * Created by workshop on 3/25/14.
 *
 * Class for Objects which will provide all necessary data of a Dynamix profile
 *
 */
public class ControlProfile {

    private String name = "";
    private String artifact_id = "";
    private String description = "";
    private String pluginVersion = "";
    private String owner_id = "";
    private String platform = "";
    private String minPlatformVersion = "";
    private String minFrameworkVersion = "";

    private SortedSet<ControllableProfileItem> inputList;
    private Map<String,String> outputList;
    private Set<String> optionalInputList;


    /**
     * Use XML string to generate equivalent ControlProfile
     *
     * @param xml XML format ControlProfile
     */
    public ControlProfile(String xml){
        XMLConverter xmlConverter = new XMLConverter();
        ControlProfile cp = xmlConverter.XML2ControlProfile(xml);
        this.setName(cp.getName());
        this.setArtifact_id(cp.getArtifact_id());
        this.setDescription(cp.getDescription());
        this.setPluginVersion(cp.getPluginVersion());
        this.setOwner_id(cp.getOwner_id());
        this.setPlatform(cp.getPlatform());
        this.setMinPlatformVersion(cp.getMinPlatformVersion());
        this.setMinFrameworkVersion(cp.getMinFrameworkVersion());
        this.inputList = cp.getInputList();
        this.outputList = cp.getOutputList();
        this.optionalInputList = cp.getOptionalInputList();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtifact_id() {
        return artifact_id;
    }

    public void setArtifact_id(String artifact_id) {
        this.artifact_id = artifact_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPluginVersion() {
        return pluginVersion;
    }

    public void setPluginVersion(String pluginVersion) {
        this.pluginVersion = pluginVersion;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getMinPlatformVersion() {
        return minPlatformVersion;
    }

    public void setMinPlatformVersion(String minPlatformVersion) {
        this.minPlatformVersion = minPlatformVersion;
    }

    public String getMinFrameworkVersion() {
        return minFrameworkVersion;
    }

    public void setMinFrameworkVersion(String minFrameworkVersion) {
        this.minFrameworkVersion = minFrameworkVersion;
    }

    public class ControllableProfileItem implements Comparable{

        private Set<String> mandatoryControls;
        private int priority;

        public ControllableProfileItem(int priority) {
            this.priority = priority;
            mandatoryControls = new HashSet<String>();
        }

        public void addMandatory(String control){
            mandatoryControls.add(control);
        }

        public int getPriority() {
            return priority;
        }

        public Set<String> getMandatoryControls() {
            return mandatoryControls;
        }

        @Override
        public int compareTo(Object o) {
            if(o instanceof ControllableProfileItem){
                ControllableProfileItem other = (ControllableProfileItem)o;
                if(priority < other.getPriority())
                    return -1;
                else if(priority > other.getPriority())
                    return 1;
                else
                    return 0;
            }
            throw new ClassCastException("Cannot compare ControllableProfileItem to " + o.getClass().getSimpleName());

        }
    }

    public ControlProfile() {
        this.inputList = new TreeSet<ControllableProfileItem>();
        this.outputList = new HashMap<String,String>();
        optionalInputList = new HashSet<String>();
    }

    public ControllableProfileItem createProfileforPriority(int priority){
        ControllableProfileItem item = new ControllableProfileItem(priority);
        inputList.add(item);
        return item;
    }

    public ControllableProfileItem createProfileforPriority(int priority, List<String> mandatoryControlls ){
        ControllableProfileItem item = new ControllableProfileItem(priority);
        for(Iterator<String> i = mandatoryControlls.iterator(); i.hasNext(); ) {
            item.addMandatory(i.next());
        }
        inputList.add(item);
        return item;
    }

    public void removeControllableProfileItem(int priority){
        for (ControllableProfileItem controllableProfile : inputList) {
            if(controllableProfile.getPriority() == priority){
                inputList.remove(controllableProfile);
            }
        }
    }

    public SortedSet<ControllableProfileItem> getInputList() {
        return inputList;
    }



    public void addOptionalControls(String control){
        optionalInputList.add(control);
    }

    public void setInputList (SortedSet<ControllableProfileItem> inputList){
        this.inputList = inputList;
    }
    public void setOutputList (Map<String,String> outputList){
        this.outputList = outputList;
    }
    public void setOptionalInputList (Set<String> optionalInputList){
        this.optionalInputList = optionalInputList;
    }

    public Set<String> getOptionalInputList() {
        return optionalInputList;
    }

    public Map<String,String> getOutputList() {
        return outputList;
    }

    public void addOutput(String name, String control){
        outputList.put(name, control);
    }

    /**
     * Returns all parameters of this object in XML-Format
     *
     * @return String in XML format
     */
    public String getXMLCode(){
        XMLConverter xmlConverter = new XMLConverter();
        return xmlConverter.ControlProfile2XML(this);
    }
}
