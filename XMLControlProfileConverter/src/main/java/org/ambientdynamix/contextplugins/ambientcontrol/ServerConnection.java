package org.ambientdynamix.contextplugins.ambientcontrol;

import org.ambientdynamix.contextplugins.test.Commands;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.ParseException;

import java.net.URI;
import java.util.*;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

/**
 * Created by Matthias Moegerle 123 on 22.07.2014.
 */

public class ServerConnection {

    public ServerConnection() {
    }

    public static final String TYPE_ID = "ids";
    public static final String TYPE_NAME = "names";
    public static final String PLUGINCONTROLDESCRIPTION = "PluginControlDescription";
    public static final String CONTROLGRAPH = "ControlGraph";
    public static final String CONFIGURATION = "configuration";
    public static final String TRANSLATOR = "Translator";
    //private static final String URL_APPLICATION = "http://205.185.117.11:8080/ControlProfileServer";
    //private static final String URL_APPLICATION = "http://localhost:8080/ControlProfileServer";

    /**
     * Writes a ControlProfile into the database
     * Converts to xml -> write to server -> write into DB
     *
     * @param controlProfile profile will be stored
     */
    public void writeInDatabase(ControlProfile controlProfile) {
        Client client = Client.create();
        WebResource service = client.resource(getBaseURI());
        String s = service.path(PLUGINCONTROLDESCRIPTION).path("ids").path(controlProfile.getArtifact_id())
                .queryParam("format", "xml")
                .type(MediaType.TEXT_PLAIN)
                .put(String.class, controlProfile.getXMLCode());
    }
    public void writeTranslatorInDatabase(String jsonTranslator, String translator_id) {
        Client client = Client.create();
        WebResource service = client.resource(getBaseURI());
        System.out.println("service "+service);
        String s = service.path(TRANSLATOR).path(translator_id)
                .queryParam("format", "json")
                .type(MediaType.TEXT_PLAIN)
                .put(String.class, jsonTranslator);
        System.out.println("s "+s);
    }

    public void deleteFromDatabase(String artifact_id) {
        Client client = Client.create();
        WebResource service = client.resource(getBaseURI());
        service.path(PLUGINCONTROLDESCRIPTION).path(TYPE_ID).path(artifact_id).delete();
    }

    public void deleteTranslatorFromDatabase(String translator_id) {
        Client client = Client.create();
        WebResource service = client.resource(getBaseURI());
        service.path(TRANSLATOR).path(translator_id).delete();
    }

    /**
     * Request all available Plugin IDs from the server DB
     *
     * @return Set of available Plugin IDs
     */
    public Set<String> getAvailableControlProfileIDs() {
        return readAllTypeFromDatabase(TYPE_ID);
    }

    /**
     * Request all available Plugin Names from the server DB
     *
     * @return Set of available Plugin Names
     */
    public Set<String> getAvailableControlProfileNames() {
        return readAllTypeFromDatabase(TYPE_NAME);
    }

    /**
     * Read form server which profiles are available
     * Returns in type of the requested TYPE (TYPE_ID or TYPE_NAME)
     *
     * @param type TYPE_ID or TYPE_NAME
     * @return Set of stored TYPE_ID or TYPE_NAME
     */
    private Set<String> readAllTypeFromDatabase(String type) {
        String outputFromDatabase = readFromDatabase(type, "");
        JSONParser parser = new JSONParser();
        Set<String> set = new HashSet<String>();
        try {
            Object obj = parser.parse(outputFromDatabase);
            JSONArray arrayList = (JSONArray) obj;
            set.addAll(arrayList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return set;
    }

    public ControlProfile getControlProfile(String artifact_id) {
        String xml = readFromDatabase(TYPE_ID, artifact_id);
        ControlProfile controlProfile = new ControlProfile(xml);
        return controlProfile;
    }

    public String getControlGraphXML(String controlGraph_id) {
        String xml = readControlGraphFromDatabase(controlGraph_id);
        return xml;
    }

    /**
     * Send the request to the database server (REST)
     * IF value == specific name or ID:
     * Returns XML-String of this ControlProfile
     * <p/>
     * ELSE IF value == ""
     * Returns a list of all Plugins in json format
     * Entries contain TYPE_ID or TYPE_NAME as set by parameter type.
     *
     * @param type  choose between TYPE_ID or TYPE_NAME
     * @param value specific ID or NAME
     * @return result string from server-request
     */
    private String readFromDatabase(String type, String value) {
        String output = "";
        Client client = Client.create();
        WebResource service = client.resource(getBaseURI());
        if (value == null || value.equals(""))
            output = service.path(PLUGINCONTROLDESCRIPTION).path(type).queryParam("format", "json").accept(MediaType.TEXT_PLAIN).get(String.class);
        else
            output = service.path(PLUGINCONTROLDESCRIPTION).path(type).path(value).queryParam("format", "xml").accept(MediaType.TEXT_PLAIN).get(String.class);
        return output;
    }

    private String readControlGraphFromDatabase(String controlGraph_id) {
        String output = "";
        Client client = Client.create();
        WebResource service = client.resource(getBaseURI());
        output = service.path(CONTROLGRAPH).path(CONFIGURATION).path(controlGraph_id).queryParam("format", "xml").accept(MediaType.TEXT_PLAIN).get(String.class);
        return output;
    }

    /**
     * Requests all profiles from server DB and returns them as a HashMap of IDs and ControlProfiles
     *
     * @return IDs and ControlProfiles matched in HashMap
     */
    public Map<String, ControlProfile> getAllProfiles() {
        XMLConverter xmlConverter = new XMLConverter();
        Map<String, ControlProfile> profiles = new HashMap<String, ControlProfile>();
        Set<String> ids = getAvailableControlProfileIDs();
        for (String id : ids) {
            String xml = readFromDatabase(ServerConnection.TYPE_ID, id);
            ControlProfile cp = xmlConverter.XML2ControlProfile(xml);
            profiles.put(id, cp);
        }
        return profiles;
    }

    /**
     * Test database by requesting all names from server database
     *
     * @param args
     */
    public static void main(String[] args) {
        ServerConnection sc = new ServerConnection();
        sc.generateProfilesFromJava();

        //Include Translator

        System.out.println("ControlGraph conf1: "+sc.getControlGraphXML("conf1"));

        String translator = "{\"source_commandItem\":\"SENSOR_PYR\",\"translator_id\":\"org.ambientdynamix.contextplugins.ambientcontrol.HeadingToColorTranslator\",\"target_commandItem\":\"DISPLAY_COLOR\"}";
        sc.writeTranslatorInDatabase(translator,"org.ambientdynamix.contextplugins.ambientcontrol.HeadingToColorTranslator");
        //sc.deleteTranslatorFromDatabase("1org.ambientdynamix.contextplugins.ambientcontrol.HeadingToColorTranslator");

        String toggleSwitchTranslator = "{\"source_commandItem\":\"SENSOR_TOGGLE\",\"translator_id\":\"org.ambientdynamix.contextplugins.ambientcontrol.ToggleCommandSwitchTranslator\",\"target_commandItem\":\"SWITCH_ON\"}";
        sc.writeTranslatorInDatabase(toggleSwitchTranslator,"org.ambientdynamix.contextplugins.ambientcontrol.ToggleCommandSwitchTranslator");

        System.exit(0);

        sc.deleteFromDatabase("12345");
        sc.deleteFromDatabase("hallo123");



    }

    public static URI getBaseURI() {
        //return UriBuilder.fromUri("http://localhost:8080/ControlProfileServer-1.0.0").build();
        return UriBuilder.fromUri("http://205.185.117.11:8080/ControlProfileServer-1.0.0").build();
    }

    public void generateProfilesFromJava() {
        ControlProfile spheroProfile;
        ControlProfile arDroneProfile;
        ControlProfile ambientMediaProfile;
        ControlProfile pitchControllerProfile;
        ControlProfile artNetLightControl;
        ControlProfile testProfile;
        ControlProfile hueProfile;
        ControlProfile wemoProfile;

        spheroProfile = new ControlProfile();
        arDroneProfile = new ControlProfile();
        ambientMediaProfile = new ControlProfile();
        pitchControllerProfile = new ControlProfile();
        artNetLightControl = new ControlProfile();
        testProfile = new ControlProfile();
        hueProfile = new ControlProfile();
        wemoProfile = new ControlProfile();

        spheroProfile.setPluginVersion("1.0.0");
        arDroneProfile.setPluginVersion("1.0.0");
        ambientMediaProfile.setPluginVersion("1.0.0");
        pitchControllerProfile.setPluginVersion("1.0.0");
        artNetLightControl.setPluginVersion("1.0.0");
        testProfile.setPluginVersion("1.0.0");
        hueProfile.setPluginVersion("1.0.0");
        wemoProfile.setPluginVersion("1.0.1");

        Map<String, ControlProfile> profiles = new HashMap<String, ControlProfile>();
        profiles.put("org.ambientdynamix.contextplugins.spheronative",
                spheroProfile);
        spheroProfile
                .setArtifact_id("org.ambientdynamix.contextplugins.spheronative");
        spheroProfile.setName("spheronative");
        profiles.put("org.ambientdynamix.contextplugins.ardrone",
                arDroneProfile);
        arDroneProfile
                .setArtifact_id("org.ambientdynamix.contextplugins.ardrone");
        arDroneProfile.setName("ardrone");
        profiles.put("org.ambientdynamix.contextplugins.ambientmedia",
                ambientMediaProfile);
        ambientMediaProfile
                .setArtifact_id("org.ambientdynamix.contextplugins.ambientmedia");
        ambientMediaProfile.setName("ambientmedia");
        profiles.put("org.ambientdynamix.contextplugins.pitchtracker",
                pitchControllerProfile);
        pitchControllerProfile
                .setArtifact_id("org.ambientdynamix.contextplugins.pitchtracker");
        pitchControllerProfile.setName("pitchtracker");
        profiles.put("org.ambientdynamix.contextplugins.simpleartnetplugin",
                artNetLightControl);
        artNetLightControl
                .setArtifact_id("org.ambientdynamix.contextplugins.simpleartnetplugin");
        artNetLightControl.setName("simpleartnetplugin");
        profiles.put("org.ambientdynamix.contextplugins.testProfile",
                testProfile);
        testProfile
                .setArtifact_id("org.ambientdynamix.contextplugins.testProfile");
        testProfile.setName("testProfile");
        profiles.put("org.ambientdynamix.contextplugins.hueplugin",
                hueProfile);
        hueProfile
                .setArtifact_id("org.ambientdynamix.contextplugins.hueplugin");
        hueProfile.setName("hueplugin");
        profiles.put("org.ambientdynamix.contextplugins.hueplugin",
                hueProfile);
        wemoProfile
                .setArtifact_id("org.ambientdynamix.contextplugins.wemoplugin");
        wemoProfile.setName("wemoplugin");
        profiles.put("org.ambientdynamix.contextplugins.hueplugin",
                wemoProfile);

        spheroProfile.addOptionalControls(Commands.DISPLAY_COLOR);
        testProfile.addOptionalControls(Commands.DISPLAY_COLOR);

        ControlProfile.ControllableProfileItem ideal = spheroProfile
                .createProfileforPriority(1);
        ideal.addMandatory(Commands.SENSOR_PYR);

        ControlProfile.ControllableProfileItem ideal2 = spheroProfile
                .createProfileforPriority(2);
        ideal2.addMandatory(Commands.SENSOR_AXIS);

        ControlProfile.ControllableProfileItem ideal3 = testProfile
                .createProfileforPriority(1);
        ideal3.addMandatory(Commands.SENSOR_PYR);

        ControlProfile.ControllableProfileItem minimum = spheroProfile
                .createProfileforPriority(3);
        minimum.addMandatory(Commands.MOVEMENT_BACKWARD);
        minimum.addMandatory(Commands.MOVEMENT_BACKWARD_LEFT);
        minimum.addMandatory(Commands.MOVEMENT_BACKWARD_RIGHT);
        minimum.addMandatory(Commands.MOVEMENT_FORWARD);
        minimum.addMandatory(Commands.MOVEMENT_FORWARD_LEFT);
        minimum.addMandatory(Commands.MOVEMENT_FORWARD_RIGHT);
        minimum.addMandatory(Commands.MOVEMENT_LEFT);
        minimum.addMandatory(Commands.MOVEMENT_RIGHT);

        spheroProfile.addOutput("Gyroscope", Commands.SENSOR_GYRO);
        spheroProfile.addOutput("Pitch Yaw Roll", Commands.SENSOR_PYR);
        spheroProfile.addOutput("Accelerometer", Commands.SENSOR_ACC);
        // spheroProfile.addAvailableControl(Commands.DISPLAY_COLOR);
        spheroProfile.addOutput("Collision", Commands.SENSOR_TOGGLE);

        testProfile.addOutput("Collision", Commands.SENSOR_TOGGLE);

        arDroneProfile.addOptionalControls(Commands.SENSOR_ACC);
        arDroneProfile.addOptionalControls(Commands.MOVEMENT_UP);
        arDroneProfile.addOptionalControls(Commands.MOVEMENT_DOWN);

        ControlProfile.ControllableProfileItem droneIdeal = arDroneProfile
                .createProfileforPriority(1);
        droneIdeal.addMandatory(Commands.SENSOR_PYR);
        droneIdeal.addMandatory(Commands.MOVEMENT_START_STOP);

        ControlProfile.ControllableProfileItem droneIdeal2 = arDroneProfile
                .createProfileforPriority(2);
        droneIdeal2.addMandatory(Commands.SENSOR_AXIS);
        droneIdeal.addMandatory(Commands.MOVEMENT_START_STOP);

        ControlProfile.ControllableProfileItem droneMinimum = arDroneProfile
                .createProfileforPriority(3);
        droneMinimum.addMandatory(Commands.MOVEMENT_BACKWARD);
        droneMinimum.addMandatory(Commands.MOVEMENT_BACKWARD_LEFT);
        droneMinimum.addMandatory(Commands.MOVEMENT_BACKWARD_RIGHT);
        droneMinimum.addMandatory(Commands.MOVEMENT_FORWARD);
        droneMinimum.addMandatory(Commands.MOVEMENT_FORWARD_LEFT);
        droneMinimum.addMandatory(Commands.MOVEMENT_FORWARD_RIGHT);
        droneMinimum.addMandatory(Commands.MOVEMENT_LEFT);
        droneMinimum.addMandatory(Commands.MOVEMENT_RIGHT);
        droneMinimum.addMandatory(Commands.MOVEMENT_START_STOP);

        ControlProfile.ControllableProfileItem hueIdeal = hueProfile
                .createProfileforPriority(1);
        hueIdeal.addMandatory(Commands.DISPLAY_COLOR);


        ControlProfile.ControllableProfileItem wemoIdeal = wemoProfile
                .createProfileforPriority(1);
        wemoIdeal.addMandatory(Commands.SWITCH_ON);
//        wemoIdeal.addMandatory(Commands.SWITCH_OFF);
        arDroneProfile.addOutput("Accelerometer", Commands.SENSOR_ACC);
        arDroneProfile.addOutput("Pitch Yaw Roll", Commands.SENSOR_PYR);
        arDroneProfile.addOutput("Gyroscope", Commands.SENSOR_GYRO);

        ambientMediaProfile.addOptionalControls(Commands.DISPLAY_IMAGE);
        ambientMediaProfile.addOptionalControls(Commands.DISPLAY_VIDEO);
        ambientMediaProfile
                .addOptionalControls(Commands.PLAYBACK_BACKWARD_SEEK);
        ambientMediaProfile.addOptionalControls(Commands.PLAYBACK_FORWARD_SEEK);
        ambientMediaProfile.addOptionalControls(Commands.PLAYBACK_NEXT);
        ambientMediaProfile.addOptionalControls(Commands.PLAYBACK_PREVIOUS);

        ControlProfile.ControllableProfileItem mediaminimum = ambientMediaProfile
                .createProfileforPriority(1);
        mediaminimum.addMandatory(Commands.PLAYBACK_PLAY_PAUSE);

        pitchControllerProfile.addOutput("Pitch", Commands.SENSOR_SINGLE_VALUE);
        pitchControllerProfile.addOutput("Loudness",
                Commands.SENSOR_SINGLE_VALUE);
        pitchControllerProfile.addOutput("First Derivative",
                Commands.SENSOR_SINGLE_VALUE);
        pitchControllerProfile.addOutput("Second Derivative",
                Commands.SENSOR_SINGLE_VALUE);

        ControlProfile.ControllableProfileItem artnetIdeal = artNetLightControl
                .createProfileforPriority(1);
        artnetIdeal.addMandatory((Commands.DISPLAY_COLOR));

        for (ControlProfile controlProfile : profiles.values()) {
            this.writeInDatabase(controlProfile);
        }

    }
}
