package org.ambientdynamix.contextplugins.ambientcontrol;

import org.ambientdynamix.*;
import org.jibx.runtime.*;

import java.io.*;
import java.util.*;

import org.ambientdynamix.ControllableProfileItemContainer;
import org.ambientdynamix.ControlCommand;

/**
 * Created by MsMe on 21.07.2014.
 */
public class XMLConverter {

    /**
     * Unmarshal xml to return profile in ControlProfile format
     * @param xml XML-String
     * @return ControlProfile Object
     */
    public ControlProfile XML2ControlProfile (String xml) {
        try {
            IBindingFactory bfact = BindingDirectory.getFactory(PluginControlDescriptionContainer.class);
            IUnmarshallingContext uctx = bfact.createUnmarshallingContext();
            StringReader stringReader = new StringReader(xml);
            Object obj = uctx.unmarshalDocument(stringReader, null);
            PluginControlDescriptionContainer profile = (PluginControlDescriptionContainer) obj;

            ControlProfile cp = new ControlProfile();

            cp.setName(profile.getName());
            cp.setArtifact_id(profile.getArtifactId());
            cp.setDescription(profile.getDescription());
            cp.setPluginVersion(profile.getPluginVersion());
            cp.setOwner_id(profile.getOwnerId());
            cp.setPlatform(profile.getPlatform());
            cp.setMinPlatformVersion(profile.getMinPlatformVersion());
            cp.setMinFrameworkVersion(profile.getMinFrameworkVersion());

            //ControllableProfileList
            if(profile.getControllableProfileList() != null) {
                List<ControllableProfileItemContainer> containerProfilItems = profile.getControllableProfileList();
                for (ControllableProfileItemContainer i : containerProfilItems) {
                    cp.createProfileforPriority(i.getPriority(), i.getMandatoryControlList());
                }
            }

            //Add available Controls
            if(profile.getAvailableControlList() != null) {
                for (ControlCommand i : profile.getAvailableControlList()) {
                    cp.addOutput(i.getName(), i.getCommand());
                }
            }

            //Add optional Controls
            if(profile.getOptionalControlList() != null) {
                for (String i : profile.getOptionalControlList()) {
                    cp.addOptionalControls(i);
                }
            }
            return cp;

        } catch (JiBXException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Marshal ControlProfile to return profile in XML format
     * @param controlProfile ControlProfile Object
     * @return String in XML format
     */
    public String ControlProfile2XML (ControlProfile controlProfile) {
        PluginControlDescriptionContainer cpc = new PluginControlDescriptionContainer();

        cpc.setName(controlProfile.getName());
        cpc.setArtifactId(controlProfile.getArtifact_id());
        cpc.setDescription(controlProfile.getDescription());
        cpc.setPluginVersion(controlProfile.getPluginVersion());
        cpc.setOwnerId(controlProfile.getOwner_id());
        cpc.setPlatform(controlProfile.getPlatform());
        cpc.setMinPlatformVersion(controlProfile.getMinPlatformVersion());
        cpc.setMinFrameworkVersion(controlProfile.getMinFrameworkVersion());

       //ControllableProfileList
        List <ControllableProfileItemContainer> containerProfileItems = cpc.getControllableProfileList();
        for(ControlProfile.ControllableProfileItem i : controlProfile.getInputList()) {
            ControllableProfileItemContainer cpic = new ControllableProfileItemContainer();
            Set<String> cSet = i.getMandatoryControls();
            List<String> list = new ArrayList<String>(cSet);
            cpic.setMandatoryControlList(list);
            cpic.setPriority(i.getPriority());
            containerProfileItems.add(cpic);
        }
        cpc.setControllableProfileList(containerProfileItems);

        //Add available Controlls
        List<ControlCommand> controlCommands = new ArrayList<ControlCommand>();
        for(Map.Entry<String, String> entry : controlProfile.getOutputList().entrySet()) {
            ControlCommand cc = new ControlCommand();
            cc.setName(entry.getKey());
            cc.setCommand(entry.getValue());
            controlCommands.add(cc);
        }
        cpc.setAvailableControlList(controlCommands);

        //Add optional Controls
        List<String> optionalControls = new ArrayList<String>(controlProfile.getOptionalInputList());
        cpc.setOptionalControlList(optionalControls);
        String returnString = "";
        try {
            IBindingFactory bfact = BindingDirectory.getFactory(PluginControlDescriptionContainer.class);
            IMarshallingContext mctx = bfact.createMarshallingContext();
            mctx.setIndent(4);
            StringWriter stringWriter = new StringWriter();

            mctx.marshalDocument(cpc, "UTF-8", null, stringWriter);

            returnString = stringWriter.toString();
        } catch (JiBXException e) {
            e.printStackTrace();
        }
        return returnString;
    }
}
