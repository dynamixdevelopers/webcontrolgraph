package org.ambientdynamix.contextplugins.test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.ambientdynamix.contextplugins.ambientcontrol.*;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import javax.ws.rs.core.MediaType;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedSet;


public class UnitTest {
    public static void main(String[] args) {
            Result result = JUnitCore.runClasses(UnitTest.class);
            for (Failure failure : result.getFailures()) {
                System.out.println(failure.toString());
            }
    }

    @Test
    public void insertDelete() {
        ControlProfile controlProfile = controlProfile("artifact_id_1");

        ServerConnection serverConnection = new ServerConnection();
        // Tests
        serverConnection.writeInDatabase(controlProfile);
		assertTrue("ControlProfile stored on server and read available artifact_id", serverConnection.getAvailableControlProfileIDs().contains(controlProfile.getArtifact_id()));
		System.out.println("Insert: Checked");

        ControlProfile serverControlProfile = serverConnection.getControlProfile(controlProfile.getArtifact_id());
        assertEquals("Server Object equals local object",controlProfile.getArtifact_id(), serverControlProfile.getArtifact_id());
        assertEquals("Server Object equals local object",controlProfile.getName(), serverControlProfile.getName());
        assertEquals("Server Object equals local object",controlProfile.getDescription(), serverControlProfile.getDescription());
        assertEquals("Server Object equals local object",controlProfile.getPluginVersion(), serverControlProfile.getPluginVersion());
        assertEquals("Server Object equals local object",controlProfile.getXMLCode().length(), serverControlProfile.getXMLCode().length());
        System.out.println("Server Object equals Local Object: Checked");

        serverConnection.deleteFromDatabase(controlProfile.getArtifact_id());
        assertTrue("ControlProfile deleted from server and read available artifact_id", !serverConnection.getAvailableControlProfileIDs().contains(controlProfile.getArtifact_id()));
        System.out.println("Delete: Checked");
    }

    private ControlProfile controlProfile(String artifact_id) {
        ControlProfile controlProfile = new ControlProfile();
        controlProfile.setOptionalInputList(new HashSet<String>());
        controlProfile.addOptionalControls("Optional_Input1");
        controlProfile.addOptionalControls("Optional_Input2");
        controlProfile.addOptionalControls("Optional_Input3");
        controlProfile.setOutputList(new HashMap<String, String>());
        controlProfile.addOutput("Output1_name", "Output1_control");
        controlProfile.addOutput("Output2_name", "Output2_control");
        SortedSet<ControlProfile.ControllableProfileItem> inputList = controlProfile.getInputList();
        ControlProfile.ControllableProfileItem cpi1 = controlProfile.createProfileforPriority(1);
        cpi1.addMandatory("Input1_prio1");
        cpi1.addMandatory("Input2_prio1");
        inputList.add(cpi1);
        ControlProfile.ControllableProfileItem cpi2 = controlProfile.createProfileforPriority(2);
        cpi2.addMandatory("Input1_prio2");
        cpi2.addMandatory("Input2_prio2");
        inputList.add(cpi2);
        controlProfile.setInputList(inputList);
        controlProfile.setArtifact_id(artifact_id);
        controlProfile.setPluginVersion("1.0.0");
        controlProfile.setDescription("description_1");
        return controlProfile;
    }
    @Test
    public void testQueries(){


        System.out.println("");
        ControlProfile controlProfile = controlProfile("artifact_id_1");
        ServerConnection serverConnection = new ServerConnection();
        serverConnection.writeInDatabase(controlProfile);

        Client client = Client.create();
        WebResource service = client.resource(ServerConnection.getBaseURI());

        System.out.println("/ControlProfileServer/REST/ids?Input={Input1_prio1, Input2_prio1}");
        String listJSONInput = service.path(ServerConnection.PLUGINCONTROLDESCRIPTION).path(ServerConnection.TYPE_ID).queryParam("Input", "{Input1_prio1, Optional_Input3}").accept(MediaType.TEXT_PLAIN).get(String.class);
        assertEquals("Found available Input",listJSONInput,"[\"artifact_id_1\"]");
        System.out.println("Query Input: Checked");

        System.out.println("/ControlProfileServer/REST/ids?Output={Output1_control}");
        String listJSONOutput = service.path(ServerConnection.PLUGINCONTROLDESCRIPTION).path(ServerConnection.TYPE_ID).queryParam("Output","{Output1_control}").accept(MediaType.TEXT_PLAIN).get(String.class);
        assertEquals("Found available Input","[\"artifact_id_1\"]",listJSONOutput);
        System.out.println("/ControlProfileServer/REST/ids?Output={Output1_control}&Constraint=\"ONE\"");
        listJSONOutput = service.path(ServerConnection.PLUGINCONTROLDESCRIPTION).path(ServerConnection.TYPE_ID).queryParam("Output","{Output1_control}").queryParam("Constraint","ONE").accept(MediaType.TEXT_PLAIN).get(String.class);
        assertEquals("Found available Input","[\"artifact_id_1\"]",listJSONOutput);
        System.out.println("/ControlProfileServer/REST/ids?Output={Output1_control,Output2_control}&Constraint=\"ALL\"");
        listJSONOutput = service.path(ServerConnection.PLUGINCONTROLDESCRIPTION).path(ServerConnection.TYPE_ID).queryParam("Output","{Output1_control,Output2_control}").queryParam("Constraint","ALL").accept(MediaType.TEXT_PLAIN).get(String.class);
        assertEquals("Found available Input","[\"artifact_id_1\"]",listJSONOutput);
        System.out.println("/ControlProfileServer/REST/ids?Output={Output1_control,Output2_control,Output3_control}&Constraint=\"ALL\"");
        listJSONOutput = service.path(ServerConnection.PLUGINCONTROLDESCRIPTION).path(ServerConnection.TYPE_ID).queryParam("Output","{Output1_control,Output2_control,Output3_control}").queryParam("Constraint","ALL").accept(MediaType.TEXT_PLAIN).get(String.class);
        assertEquals("Found available Input","",listJSONOutput);


        ControlProfile controlProfile1 = controlProfile("artifact_id_2");
        serverConnection.writeInDatabase(controlProfile1);

        System.out.println("/ControlProfileServer/REST/ids?Input={Input1_prio1, Input2_prio1}&Output={Output1_control,Output2_control}&Constraint=\"ALL\"");
        listJSONOutput = service.path(ServerConnection.PLUGINCONTROLDESCRIPTION).path(ServerConnection.TYPE_ID).queryParam("Output","{Output1_control,Output2_control,Output3_control}").queryParam("Constraint","ALL").accept(MediaType.TEXT_PLAIN).get(String.class);
        assertEquals("Found available Input","",listJSONOutput);
        System.out.println("Query Output: Checked");

        serverConnection.deleteFromDatabase(controlProfile1.getArtifact_id());
        serverConnection.deleteFromDatabase(controlProfile.getArtifact_id());
    }

    @Test
    public void testURL(){
        System.out.println("");
        ControlProfile controlProfile = controlProfile("artifact_id_1");
        ServerConnection serverConnection = new ServerConnection();
        serverConnection.writeInDatabase(controlProfile);

        Client client = Client.create();
        WebResource service = client.resource(ServerConnection.getBaseURI());

        System.out.println("/PluginControlDescprition/ids?format={format}");
        String listJSON = service.path(ServerConnection.PLUGINCONTROLDESCRIPTION).path(ServerConnection.TYPE_ID).queryParam("format", "json").accept(MediaType.TEXT_PLAIN).get(String.class);
        System.out.println("JSON: "+listJSON);
        //String listXML = service.path(ServerConnection.PLUGINCONTROLDESCRIPTION).path(ServerConnection.TYPE_ID).queryParam("format", "xml").accept(MediaType.TEXT_PLAIN).get(String.class);
        //System.out.println("XML: "+listXML);
        System.out.println("");

        System.out.println("/PluginControlDescprition/ids/"+controlProfile.getArtifact_id()+"?format={format}");
        String profileJSON = service.path(ServerConnection.PLUGINCONTROLDESCRIPTION).path(ServerConnection.TYPE_ID).path(controlProfile.getArtifact_id()).queryParam("format", "json").accept(MediaType.TEXT_PLAIN).get(String.class);
        System.out.println("JSON: "+profileJSON);
        String profileXML = service.path(ServerConnection.PLUGINCONTROLDESCRIPTION).path(ServerConnection.TYPE_ID).path(controlProfile.getArtifact_id()).queryParam("format", "xml").accept(MediaType.TEXT_PLAIN).get(String.class);
        assertEquals("Profile in XML format",controlProfile.getXMLCode().length(),profileXML.length());
        System.out.println("XML: "+profileXML);
        System.out.println("");

        System.out.println("/PluginControlDescprition/names/"+controlProfile.getName()+"?format={format}");
        profileJSON = service.path(ServerConnection.PLUGINCONTROLDESCRIPTION).path(ServerConnection.TYPE_NAME).path(controlProfile.getName()).queryParam("format", "json").accept(MediaType.TEXT_PLAIN).get(String.class);
        System.out.println("JSON: "+profileJSON);
        profileXML = service.path(ServerConnection.PLUGINCONTROLDESCRIPTION).path(ServerConnection.TYPE_ID).path(controlProfile.getArtifact_id()).queryParam("format", "xml").accept(MediaType.TEXT_PLAIN).get(String.class);
        assertEquals("Profile in XML format",controlProfile.getXMLCode().length(),profileXML.length());
        System.out.println("XML: "+profileXML);
        System.out.println("");

        serverConnection.deleteFromDatabase(controlProfile.getArtifact_id());
    }

}
