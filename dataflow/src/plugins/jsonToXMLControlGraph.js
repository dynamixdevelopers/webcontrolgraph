
function jsonToXML (jsonData){
	console.log("jsonToXML");
	receiverArray = [];
	console.log(jsonData);
	return edgeToXML(jsonData)
}

var receiverArray = [];
function edgeToXML(jsonData){
	edges = jsonData["edges"];
	nodes = jsonData.nodes;
	
	edges.forEach(function(edge){
		//console.log(edge.source.port);
		//console.log(edge.target.port);
		//console.log(edge.source);
		//console.log(edge.source.node);

        //commandType target
        commandType_target = edge.target.port;

		//commandType_source
        source_name = edge.source.port;
        commandType_source = source_name.substring(source_name.lastIndexOf("(")+1,source_name.lastIndexOf(")"));

        console.log("hallo");
		console.log(edge);
		
		//name
		//name = edge.source.port;
		source_name_text = source_name.substring(0, source_name.lastIndexOf("(")-1);
        console.log(source_name_text);

		//sourcePlugin
		sourcePlugin = getNodeArtifact_Id(edge.source.node, nodes)
		//console.log(sourcePlugin); 
		
		//targetPlugin
		targetPlugin = getNodeArtifact_Id(edge.target.node, nodes)
		//console.log(targetPlugin); 
		//receiverArray.push(targetPlugin);


		//controlEdge
		controlEdge = getControlEdge(source_name_text, commandType_source,commandType_target, sourcePlugin, targetPlugin)
		//console.log(controlEdge);
		
		addReceiver(targetPlugin, sourcePlugin, controlEdge);

	});
	
	return getFinalXML(receiverArray);
}

function getFinalXML(receiverArray){
	header = '<?xml version="1.0" encoding="UTF-8"?>';
	var controlGraph = "";
	receiverArray.forEach(function (receiver){
	    receiverStr = "<receiver>"+receiver.artifact_id+"</receiver>";
	    controllerStr = "";
	    receiver.controller.forEach(function(controller){
	    	controllerStr = controllerStr + "<controller>"+controller+"</controller>";
	    });
	    controlEdgeStr = "";
	    receiver.controlEdge.forEach(function (controlEdge){
	    	controlEdgeStr = controlEdgeStr + controlEdge;
	    });

    var endString = receiverStr + controllerStr + controlEdgeStr;
    controlGraph = controlGraph + endString;
	});

    controlGraph = header + "<ControlConfig>" + controlGraph + "</ControlConfig>";
	console.log(controlGraph);
	return controlGraph;

	
}

function addReceiver(receiverArtifact_Id, sourceArtifact_id, controlEdge){
	found = false;
	receiverArray.forEach(function (receiver){
		if(receiver.artifact_id === receiverArtifact_Id){
			if($.inArray(sourceArtifact_id, receiver.controller) === -1){
				receiver.controller.push(sourceArtifact_id);
			}
			receiver.controlEdge.push(controlEdge);
			found = true;
			return false;
		}
	});
	if(found){
		return false;
	}
	var myReceiver = {artifact_id:"",controller:[], controlEdge:[]};
	myReceiver.artifact_id = receiverArtifact_Id;
	myReceiver.controller = [];
	myReceiver.controller.push(sourceArtifact_id);
	myReceiver.controlEdge.push(controlEdge);

	receiverArray.push(myReceiver);

}

function getNodeArtifact_Id(id, nodes){
	var ret = 0;
	nodes.forEach(function(node){
		if(node.id === id){
			ret = node.artifact_id;
		}
	});
	return ret;
}


function getControlEdge(name, commandType_source, commandType_target, sourcePlugin, targetPlugin){
	xmlCommandType_source = "<commandType>"+commandType_source+"</commandType>";
	xmlName = "<name>"+name+"</name>";
	xmlSourcePlugin = "<sourcePlugin>"+sourcePlugin+"</sourcePlugin>";
	xmlTargetPlugin = "<targetPlugin>"+targetPlugin+"</targetPlugin>";
    translator_xml = "";
    if(commandType_source === commandType_target){
    }else{
        translator_id = getTranslatorID(commandType_source, commandType_target);
        if(translator_id === "")
            alert("No Translator "+commandType_source+" <-> "+commandType_target+" available!");
        else{
            translator_xml = "<translator>"+"<translateTo>"+commandType_target+"</translateTo>"+"<class>"+translator_id+"</class>"+
                "</translator>";
        }
    }
	controlEdge = "<controlEdge>"+xmlCommandType_source+xmlName+xmlSourcePlugin+xmlTargetPlugin+translator_xml+"</controlEdge>";
    return controlEdge;
}

function getTranslatorID(sourceCommand, targetCommand){
    var translator_id = "";
    $.ajax({
        url: '../ControlProfileServer-1.0.0/'+"Translator"+'?Source='+sourceCommand+'&Target='+targetCommand,
        async:false,
        success: function(data) {
            translator_id = jQuery.parseJSON(data);
        }
    });
    console.log(translator_id);
    return translator_id;
}