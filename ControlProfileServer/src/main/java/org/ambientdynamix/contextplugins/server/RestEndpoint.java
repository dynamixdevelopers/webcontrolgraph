package org.ambientdynamix.contextplugins.server;

import org.ambientdynamix.contextplugins.entity.*;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServlet;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("")
public class RestEndpoint extends HttpServlet {

    public static final String ID = "ids";
    public static final String NAME = "names";
    public static final String PLUGINCONTROLDESCRIPTION = "PluginControlDescription";
    public static final String CONTROLGRAPH = "ControlGraph";
    public static final String TRANSLATOR = "Translator";

    public static final String ACTION_INSERT = "INSERT";
    public static final String ACTION_UPDATE = "UPDATE";
    public static final String ACTION_NOTHING_OUTDATED = "NOTHING_OUTDATED";
    public static final String ACTION_NOTHING_IDENTICAL = "NOTHING_INSERT";

    public static final String FORMAT_XML = "xml";
    public static final String FORMAT_JSON = "json";
    public static final String FORMAT_HTML = "html";

    /**
     * Return complete list of Artifact_IDs as JSON String format.

     * @param valueType ids or names
     * @param input List of requested inputs e.g. Input={SENSOR_PYR, MOVEMENT_UP}
     * @param output List of requested outputs e.g. Output={MOVEMENT_DOWN}
     * @param constriant Constraint regarding the List. Result should contain {ALL|ONE} of those list entries
     * @return JSON List with all IDs
     */
    @GET
    @Path("/"+PLUGINCONTROLDESCRIPTION+"/{valueType}")
    public Response getAllIds(@PathParam("valueType") String valueType, @QueryParam("Input") String input, @QueryParam("Output") String output, @QueryParam("Constraint") String constriant) {
        PluginControlDescription_DAO pcd_dao = new PluginControlDescription_DAO();
        String ret = pcd_dao.getAllArtifactIds(valueType, input, output, constriant);
        return Response.status(Response.Status.OK).entity(ret).build();
    }
    @GET
    @Path("/"+TRANSLATOR)
    public Response getAllTranslatorIds(@QueryParam("Source") String sourceCommand, @QueryParam("Target") String targetCommand) {
        Translator_DAO tr_dao = new Translator_DAO();
        String ret = tr_dao.getAllTranslatorIds(sourceCommand,targetCommand);
        return Response.status(Response.Status.OK).entity(ret).build();
    }
    @GET
    @Path("/"+CONTROLGRAPH)
    public Response getAllControlGraphIds() {
        ControlGraph cg = new ControlGraph();
        String ret = cg.getAllControlGraphIDs();
        return Response.status(Response.Status.OK).entity(ret).build();
    }

    /**
     * Request the xml|json profile of a specific artifact_id
     * Default return format without format parameter: XML
     * @param format xml or json return format
     * @param artifact_id Unique artifact_id of PluginControlProfile
     * @return xml or profile which can be converted to a ControlProfile
     */
    @GET
    @Path("/"+PLUGINCONTROLDESCRIPTION+"/" + ID + "/{id}")
    public Response getResultByID(@QueryParam("format")String format, @PathParam("id") String artifact_id) {
        PluginControlDescription_DAO pcd_dao = new PluginControlDescription_DAO();
        PluginControlDescription pcd = pcd_dao.getPCDByArtifactID(artifact_id);
        String ret = pcd.getDescription(format);
        return Response.status(Response.Status.OK).entity(ret).build();

    }

    /**
     * Request the xml|json profile of a specific name
     * Attention, names are not unique, please only use for debug purpose.
     * Default return format without format parameter: XML
     * @param format xml or json return format
     * @param name Not unique name of PluginControlProfile
     * @return xml or profile which can be converted to a ControlProfile
     */
	@GET
	@Path("/"+PLUGINCONTROLDESCRIPTION+"/" + NAME + "/{name}")
	public Response getResultByName(@PathParam("name") String name, @QueryParam("format") String format) {
        PluginControlDescription_DAO pcd_dao = new PluginControlDescription_DAO();
        PluginControlDescription pcd = pcd_dao.getPCDByName(name);
        String ret = pcd.getDescription(format);
		return Response.status(Response.Status.OK).entity(ret).build();
	}

    /**
     * Get ControlGraph description by ID
     * @param format json (for MeemooInterface), XML (for Ambient Dynamix interface)
     *
     * @param controlGraph_id unique controlGraph ID
     * @return json or xml description of ControlGraph
     */
	@GET
	@Path("/"+CONTROLGRAPH+"/{id}")
	public Response getControlGraphByID(@QueryParam("format")String format, @PathParam("id") String controlGraph_id) {
        ControlGraph cg = new ControlGraph();
        String ret = cg.getById(controlGraph_id, format);
		return Response.status(Response.Status.OK).entity(ret).build();
	}

    @GET
    @Path("/"+TRANSLATOR + "/{id}")
    public Response getTranslator(@QueryParam("format")String format, @PathParam("id") String translator_id) {
        Translator_DAO tr_dao = new Translator_DAO();
        Translator tr = tr_dao.getTranslaor(translator_id);
        String ret = tr.getJSONFormat();
        return Response.status(Response.Status.OK).entity(ret).build();
    }

    @GET
    @Path("/"+TRANSLATOR+"1")
    public Response getTranslator1(@QueryParam("format")String format, @PathParam("id") String translator_id) {
        EntityManager entityManager = DatabaseMgr.getEntityManager();
        entityManager.getTransaction().begin();

        Translator translator = new Translator("org.test."+format);
        entityManager.persist(translator);

        CommandItem ci1 = new CommandItem("a");
        translator.setSourceCommandItem(entityManager, ci1);

        CommandItem ci2 = new CommandItem("b");
        translator.setTargetCommandItem(entityManager, ci2);

        System.out.println("translator_id: "+translator.getTranslatorID());
        //entityManager.persist(translator);
        //translator = entityManager.merge(translator);

        entityManager.getTransaction().commit();
        DatabaseMgr.close();

        String ret = "done";

        /*Translator_DAO tr_dao = new Translator_DAO();
        Translator translator = new Translator("org.test.test.test."+format,new CommandItem("a"),new CommandItem("b"));
        tr_dao.putTranslator(translator);
        System.out.println("PUT");
        String ret = "";*/
        return Response.status(Response.Status.OK).entity(ret).build();
    }


    /**
     * Put new PluginControlDescription to Database
     *
     * @param artifact_id Unique artifact_id
     * @param description Description of PluginControl in XML or JSON format
     * @param format to declare the used format, xml or json
     * @return
     */
	@PUT
	@Consumes(MediaType.TEXT_PLAIN)
	@Path("/"+PLUGINCONTROLDESCRIPTION+"/" + ID + "/{id}")
	public Response put( @PathParam("id") String artifact_id, String description, @QueryParam("format")String format) {
        PluginControlDescription_DAO pcd_dao = new PluginControlDescription_DAO();
        System.out.println("PUT");
        PluginControlDescription pcd = pcd_dao.putPCD(description, format);
        String ret = "";
		return Response.status(Response.Status.OK).entity(ret).build();
	}

    @PUT
     @Consumes(MediaType.TEXT_PLAIN)
     @Path("/"+TRANSLATOR+ "/{id}")
     public Response putTranslator( @PathParam("id") String translator_id, String description, @QueryParam("format")String format) {
        Translator_DAO tr_dao = new Translator_DAO();

        Translator translator = tr_dao.putTranslatorFromJSON(description);

        System.out.println("PUT");
        String ret = "";
        return Response.status(Response.Status.OK).entity(ret).build();
    }

    /**
     * Put new ControlGraph to Database
     * @param id unique id of ControlGraph
     * @param controlGraph description of ControlGraph in declared format
     * @param format declared format type, xml or json
     * @return
     */
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/"+CONTROLGRAPH+"/{id}")
	public Response putControlGraph( @PathParam("id") String id, String controlGraph, @QueryParam("format")String format) {
		ControlGraph cg = new ControlGraph();
        System.out.println("controlGraph: "+controlGraph);
		cg = cg.addControlGraph(id, controlGraph, format);
        String ret = "PUT";
        return Response.status(Response.Status.OK).entity(ret).build();
	}

    /**
     * POST, update PluginControlDescription at Database
     *
     * @param artifact_id
     * @param description updated description
     * @param format format of description (xml or json)
     * @return
     */
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Path("/"+PLUGINCONTROLDESCRIPTION+"/" + ID + "/{id}")
	public Response post( @PathParam("id") String artifact_id, String description, @QueryParam("format")String format) {
        PluginControlDescription_DAO pcd_dao = new PluginControlDescription_DAO();
        System.out.println("POST");
        PluginControlDescription pcd = pcd_dao.postPCD(description, format);
        String ret = pcd.getDescriptionXML();
		return Response.status(Response.Status.OK).entity(ret).build();
		
	}

    /**
     * POST, update ControlGraph at Database
     *
     * @param id
     * @param controlGraph updated description of ControlGraph
     * @param format format of description (xml or json)
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/"+CONTROLGRAPH+ "/{id}")
    public Response postControlGraph( @PathParam("id") String id, String controlGraph, @QueryParam("format")String format) {
        ControlGraph cg = new ControlGraph();
        cg = cg.addControlGraph(id, controlGraph, format);
        String ret = "POST";
        return Response.status(Response.Status.OK).entity(ret).build();
    }

    /**
     * Delete PluginControlDescription from Database
     *
     * @param artifact_id
     * @return
     */
	@DELETE
	@Consumes(MediaType.TEXT_PLAIN)
	@Path("/"+PLUGINCONTROLDESCRIPTION+"/" + ID + "/{id}")
	public Response deletePluginControlDescription(@PathParam("id") String artifact_id) {
        PluginControlDescription_DAO pcd_dao = new PluginControlDescription_DAO();
        PluginControlDescription pcd = pcd_dao.getPCDByArtifactID(artifact_id);
        String ret = "";
        if(pcd == null){
            ret = "Entry not in DB";
        }else {
            pcd_dao.deletePCD(pcd);
        }
        return Response.status(Response.Status.OK).entity("deleted").build();
	}

    /**
     * Delete ControlGraph from Database
     *
     * @param controlGraph_id
     * @return
     */
    @DELETE
    @Consumes(MediaType.TEXT_PLAIN)
    @Path("/"+CONTROLGRAPH + "/{id}")
    public Response deleteControlGraph(@PathParam("id") String controlGraph_id) {
        ControlGraph cg = ControlGraph.getCGById(controlGraph_id);
        String ret = "";
        if(cg == null){
            ret = "Entry not in DB";
        }else {
            cg.delete();
        }
        return Response.status(Response.Status.OK).entity("deleted").build();
    }
    /**
     * Delete Translator from Database
     *
     * @param translator_id
     * @return
     */
    @DELETE
    @Consumes(MediaType.TEXT_PLAIN)
    @Path("/"+TRANSLATOR + "/{id}")
    public Response deleteTranslation(@PathParam("id") String translator_id) {
        Translator_DAO tr_dao = new Translator_DAO();
        Translator translator = tr_dao.getTranslaor(translator_id);

        String ret = "";
        if(translator == null){
            ret = "Entry not in DB";
        }else {
            tr_dao.delete(translator);
        }
        return Response.status(Response.Status.OK).entity("deleted").build();
    }
}
