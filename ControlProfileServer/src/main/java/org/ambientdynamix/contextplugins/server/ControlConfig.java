package org.ambientdynamix.contextplugins.server;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

/**
 * Created by MsMe on 18.09.2014.
 */
public class ControlConfig {
    private String receiver = "";
    private Set<String> controller;
    private Set<ControlEdge> controlEdge;

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Set<String> getController() {
        return controller;
    }

    public void setController(Set<String> controller) {
        this.controller = controller;
    }

    public Set<ControlEdge> getControlEdge() {
        return controlEdge;
    }

    public void setControlEdge(Set<ControlEdge> controlEdge) {
        this.controlEdge = controlEdge;
    }

    public class ControlEdge{
        private String commandType = "";
        private String name = "";
        private String sourcePlugin = "";
        private String targetPlugin = "";

        public String getCommandType() {
            return commandType;
        }

        public void setCommandType(String commandType) {
            this.commandType = commandType;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSourcePlugin() {
            return sourcePlugin;
        }

        public void setSourcePlugin(String sourcePlugin) {
            this.sourcePlugin = sourcePlugin;
        }

        public String getTargetPlugin() {
            return targetPlugin;
        }

        public void setTargetPlugin(String targetPlugin) {
            this.targetPlugin = targetPlugin;
        }
    }
}
