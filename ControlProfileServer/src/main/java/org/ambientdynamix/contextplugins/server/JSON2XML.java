package org.ambientdynamix.contextplugins.server;

import com.google.gson.Gson;
import org.ambientdynamix.contextplugins.ambientcontrol.ControlProfile;

import java.util.HashSet;

/**
 * Created by MsMe on 18.09.2014.
 */
public class JSON2XML {


    public HashSet<String> receiverSet;

    public String jsonToXML (String jsonData){
        System.out.println("jsonToXML");
        receiverSet = new HashSet<String>();
        return edgeToXML(jsonData);
    }

    private String edgeToXML(String jsonData){
        Gson gson = new Gson();
        ControlConfig cg = gson.fromJson(jsonData, ControlConfig.class);

        return cg.getReceiver();
        /*
        edges = jsonData["edges"];
        nodes = jsonData.nodes;

        edges.forEach(function(edge){

            //commandType
            commandType = edge.target.port;

            //name
            name = edge.source.port
            name = name.substring(0, name.length-(commandType.length+3));

            //sourcePlugin
            sourcePlugin = getNodeArtifact_Id(edge.source.node, nodes);

            //targetPlugin
            targetPlugin = getNodeArtifact_Id(edge.target.node, nodes)

            //controlEdge
            controlEdge = getControlEdge(name, commandType, sourcePlugin, targetPlugin)

            addReceiver(targetPlugin, sourcePlugin, controlEdge);

        });

        return getFinalXML(receiverArray);
    }

    function getFinalXML(receiverArray){
        header = '<?xml version="1.0" encoding="UTF-8"?>';
        var match = "";
        receiverArray.forEach(function (receiver){
            receiverStr = "<receiver>"+receiver.artifact_id+"</receiver>";
            controllerStr = "";
            receiver.controller.forEach(function(controller){
                controllerStr = controllerStr + "<controller>"+controller+"</controller>";
            });
            controlEdgeStr = "";
            receiver.controlEdge.forEach(function (controlEdge){
                controlEdgeStr = controlEdgeStr + controlEdge;
            });

            var matching = receiverStr + controllerStr + controlEdgeStr;
            match = match + matching;
        });

        match = header + "<ControlConfig>" + match + "</ControlConfig>";
        console.log(match);
        return match;


    }

    function addReceiver(receiverArtifact_Id, sourceArtifact_id, controlEdge){
        found = false;
        receiverArray.forEach(function (receiver){
            if(receiver.artifact_id === receiverArtifact_Id){
                if($.inArray(sourceArtifact_id, receiver.controller) === -1){
                    receiver.controller.push(sourceArtifact_id);
                }
                receiver.controlEdge.push(controlEdge);
                found = true;
                return false;
            }
        });
        if(found){
            return false;
        }
        var myReceiver = {artifact_id:"",controller:[], controlEdge:[]};
        myReceiver.artifact_id = receiverArtifact_Id;
        myReceiver.controller = [];
        myReceiver.controller.push(sourceArtifact_id);
        myReceiver.controlEdge.push(controlEdge);

        receiverArray.push(myReceiver);

    }

    function getNodeArtifact_Id(id, nodes){
        var ret = 0;
        nodes.forEach(function(node){
            if(node.id === id){
                ret = node.artifact_id;
            }
        });
        return ret;
    }


    function getControlEdge(name, commandType, sourcePlugin, targetPlugin){
        xmlCommandType = "<commandType>"+commandType+"</commandType>";
        xmlName = "<name>"+name+"</name>";
        xmlSourcePlugin = "<sourcePlugin>"+sourcePlugin+"</sourcePlugin>";
        xmlTargetPlugin = "<targetPlugin>"+targetPlugin+"</targetPlugin>";
        controlEdge = "<controlEdge>"+xmlCommandType+xmlName+xmlSourcePlugin+xmlTargetPlugin+"</controlEdge>";
        return controlEdge;

     */
    }
}
