package org.ambientdynamix.contextplugins.entity;

public class Commands {

    public static final String MOVEMENT_LEFT = "MOVEMENT_LEFT";
    public static final String MOVEMENT_RIGHT = "MOVEMENT_RIGHT";
    public static final String MOVEMENT_BACKWARD = "MOVEMENT_BACKWARD";
    public static final String MOVEMENT_FORWARD = "MOVEMENT_FORWARD";
    public static final String MOVEMENT_FORWARD_RIGHT = "MOVEMENT_FORWARD_RIGHT";
    public static final String MOVEMENT_FORWARD_LEFT = "MOVEMENT_FORWARD_LEFT";
    public static final String MOVEMENT_BACKWARD_RIGHT = "MOVEMENT_BACKWARD_RIGHT";
    public static final String MOVEMENT_BACKWARD_LEFT = "MOVEMENT_BACKWARD_LEFT";
    public static final String MOVEMENT_UP = "MOVEMENT_UP";
    public static final String MOVEMENT_DOWN = "MOVEMENT_DOWN";
    public static final String MOVEMENT_START_STOP = "MOVEMENT_START_STOP";
    public static final String MOVEMENT_NEUTRAL = "MOVEMENT_NEUTRAL";
    public static final String MOVEMENT_SPIN_CCW = "MOVEMENT_SPIN_CCW";
    public static final String MOVEMENT_SPIN_CW = "MOVEMENT_SPIN_CW";
    public static final String PLAYBACK_PLAY_PAUSE = "PLAYBACK_PLAY_PAUSE";
    public static final String PLAYBACK_FORWARD_SEEK = "PLAYBACK_FORWARD_SEEK";
    public static final String PLAYBACK_BACKWARD_SEEK = "PLAYBACK_BACKWARD_SEEK";
    public static final String PLAYBACK_NEXT = "PLAYBACK_NEXT";
    public static final String PLAYBACK_PREVIOUS = "PLAYBACK_PREVIOUS";
    public static final String PLAYBACK_STOP = "PLAYBACK_STOP";
    public static final String DISPLAY_COLOR = "DISPLAY_COLOR";
    public static final String DISPLAY_IMAGE = "DISPLAY_IMAGE";
    public static final String DISPLAY_VIDEO = "DISPLAY_VIDEO";
    public static final String DISPLAY_MESSAGE = "DISPLAY_MESSAGE";
    public static final String SENSOR_PYR = "SENSOR_PYR";
    public static final String SENSOR_ACC = "SENSOR_ACC";
    public static final String SENSOR_GYRO = "SENSOR_GYRO";
    public static final String SENSOR_AXIS = "SENSOR_AXIS";
    public static final String SENSOR_TOGGLE = "SENSOR_TOGGLE";
    public static final String SENSOR_KEYPRESS = "SENSOR_KEYPRESS";
    public static final String SENSOR_SINGLE_VALUE = "SENSOR_SINGLE_VALUE";    
    public static final String TRIGGER = "TRIGGER";
    public static final String SWITCH_ON = "SWITCH_ON";
    public static final String SWITCH_OFF = "SWITCH_OFF";
}