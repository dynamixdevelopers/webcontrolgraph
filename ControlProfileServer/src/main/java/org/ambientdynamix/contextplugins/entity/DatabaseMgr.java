package org.ambientdynamix.contextplugins.entity;

import org.ambientdynamix.contextplugins.ambientcontrol.ControlProfile;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import java.util.*;

public class DatabaseMgr {

	public DatabaseMgr() {
        EntityManager myEnitiyManager = DatabaseMgr.getEntityManager();
		myEnitiyManager.getTransaction().begin();

        Translator t = new Translator("org.ambientdynamix.contextplugins.ambientcontrol.HeadingToColorTranslator");
        CommandItem ci1 = new CommandItem("SENSOR_PYR");
        if(myEnitiyManager.contains(ci1)){
            ci1 = myEnitiyManager.merge(ci1);
        }
        t.setSourceCommandItem(myEnitiyManager, ci1);
        CommandItem ci2 = new CommandItem("DISPLAY_COLOR");
        if(myEnitiyManager.contains(ci2)){
            ci2 = myEnitiyManager.merge(ci2);
        }
        t.setTargetCommandItem(myEnitiyManager, ci2);
        //t.setTargetCommandItem(new CommandItem("SENSOR_AXIS"));
        myEnitiyManager.persist(t);

        //generateProfilesFromJava(myEnitiyManager); //Insert all existing profiles to database

        //t.remove(myEnitiyManager);

		entityManager.getTransaction().commit();
        DatabaseMgr.close();

        PluginControlDescription_DAO pcd_dao = new PluginControlDescription_DAO();
        PluginControlDescription pcd1 = pcd_dao.getPCDByArtifactID("org.ambientdynamix.contextplugins.testProfile");
        PluginControlDescription pcd = pcd_dao.getPCDByArtifactID("org.ambientdynamix.contextplugins.spheronative");
        //pcd_dao.deletePCD(pcd);
        //pcd_dao.deletePCD(pcd1);

		System.exit(0);
	}

    public static void main(final String[] args) {
        //DatabaseMgr main = new DatabaseMgr();
    }

    private static final EntityManagerFactory entityManagerFactory = Persistence
            .createEntityManagerFactory("manager1");
    private static EntityManager entityManager = null;

    public static EntityManager getEntityManager() {
        entityManager = entityManagerFactory.createEntityManager();
        return entityManager;
    }

    public static void close() {
        entityManager.close();
    }

	public void generateProfilesFromJava(EntityManager session) {
        Map<String, ControlProfile> profiles;
		ControlProfile spheroProfile;
		ControlProfile arDroneProfile;
		ControlProfile ambientMediaProfile;
		ControlProfile pitchControllerProfile;
		ControlProfile artNetLightControl;
		ControlProfile testProfile;

		spheroProfile = new ControlProfile();
		arDroneProfile = new ControlProfile();
		ambientMediaProfile = new ControlProfile();
		pitchControllerProfile = new ControlProfile();
		artNetLightControl = new ControlProfile();
		testProfile = new ControlProfile();

		spheroProfile.setPluginVersion("1.0.0");
		arDroneProfile.setPluginVersion("1.0.0");
		ambientMediaProfile.setPluginVersion("1.0.0");
		pitchControllerProfile.setPluginVersion("1.0.0");
		artNetLightControl.setPluginVersion("1.0.0");
		testProfile.setPluginVersion("1.0.0");

		profiles = new HashMap<String, ControlProfile>();
		profiles.put("org.ambientdynamix.contextplugins.spheronative",
				spheroProfile);
		spheroProfile
				.setArtifact_id("org.ambientdynamix.contextplugins.spheronative");
		spheroProfile.setName("spheronative");
		profiles.put("org.ambientdynamix.contextplugins.ardrone",
				arDroneProfile);
		arDroneProfile
				.setArtifact_id("org.ambientdynamix.contextplugins.ardrone");
		arDroneProfile.setName("ardrone");
		profiles.put("org.ambientdynamix.contextplugins.ambientmedia",
				ambientMediaProfile);
		ambientMediaProfile
				.setArtifact_id("org.ambientdynamix.contextplugins.ambientmedia");
		ambientMediaProfile.setName("ambientmedia");
		profiles.put("org.ambientdynamix.contextplugins.pitchtracker",
				pitchControllerProfile);
		pitchControllerProfile
				.setArtifact_id("org.ambientdynamix.contextplugins.pitchtracker");
		pitchControllerProfile.setName("pitchtracker");
		profiles.put("org.ambientdynamix.contextplugins.simpleartnetplugin",
				artNetLightControl);
		artNetLightControl
				.setArtifact_id("org.ambientdynamix.contextplugins.simpleartnetplugin");
		artNetLightControl.setName("simpleartnetplugin");
		profiles.put("org.ambientdynamix.contextplugins.testProfile",
				testProfile);
		testProfile
				.setArtifact_id("org.ambientdynamix.contextplugins.testProfile");
		testProfile.setName("testProfile");

		spheroProfile.addOptionalControls(Commands.DISPLAY_COLOR);
		testProfile.addOptionalControls(Commands.DISPLAY_COLOR);

		ControlProfile.ControllableProfileItem ideal = spheroProfile
				.createProfileforPriority(1);
		ideal.addMandatory(Commands.SENSOR_PYR);

		ControlProfile.ControllableProfileItem ideal2 = spheroProfile
				.createProfileforPriority(2);
		ideal2.addMandatory(Commands.SENSOR_AXIS);

		ControlProfile.ControllableProfileItem ideal3 = testProfile
				.createProfileforPriority(1);
		ideal3.addMandatory(Commands.SENSOR_PYR);

		ControlProfile.ControllableProfileItem minimum = spheroProfile
				.createProfileforPriority(3);
		minimum.addMandatory(Commands.MOVEMENT_BACKWARD);
		minimum.addMandatory(Commands.MOVEMENT_BACKWARD_LEFT);
		minimum.addMandatory(Commands.MOVEMENT_BACKWARD_RIGHT);
		minimum.addMandatory(Commands.MOVEMENT_FORWARD);
		minimum.addMandatory(Commands.MOVEMENT_FORWARD_LEFT);
		minimum.addMandatory(Commands.MOVEMENT_FORWARD_RIGHT);
		minimum.addMandatory(Commands.MOVEMENT_LEFT);
		minimum.addMandatory(Commands.MOVEMENT_RIGHT);

		spheroProfile.addOutput("Gyroscope", Commands.SENSOR_GYRO);
		spheroProfile.addOutput("Pitch Yaw Roll", Commands.SENSOR_PYR);
		spheroProfile.addOutput("Accelerometer", Commands.SENSOR_ACC);
		// spheroProfile.addAvailableControl(Commands.DISPLAY_COLOR);
		spheroProfile.addOutput("Collision", Commands.SENSOR_TOGGLE);

		testProfile.addOutput("Collision", Commands.SENSOR_TOGGLE);

		arDroneProfile.addOptionalControls(Commands.SENSOR_ACC);
		arDroneProfile.addOptionalControls(Commands.MOVEMENT_UP);
		arDroneProfile.addOptionalControls(Commands.MOVEMENT_DOWN);

		ControlProfile.ControllableProfileItem droneIdeal = arDroneProfile
				.createProfileforPriority(1);
		droneIdeal.addMandatory(Commands.SENSOR_PYR);
		droneIdeal.addMandatory(Commands.MOVEMENT_START_STOP);

		ControlProfile.ControllableProfileItem droneIdeal2 = arDroneProfile
				.createProfileforPriority(2);
		droneIdeal2.addMandatory(Commands.SENSOR_AXIS);
		droneIdeal.addMandatory(Commands.MOVEMENT_START_STOP);

		ControlProfile.ControllableProfileItem droneMinimum = arDroneProfile
				.createProfileforPriority(3);
		droneMinimum.addMandatory(Commands.MOVEMENT_BACKWARD);
		droneMinimum.addMandatory(Commands.MOVEMENT_BACKWARD_LEFT);
		droneMinimum.addMandatory(Commands.MOVEMENT_BACKWARD_RIGHT);
		droneMinimum.addMandatory(Commands.MOVEMENT_FORWARD);
		droneMinimum.addMandatory(Commands.MOVEMENT_FORWARD_LEFT);
		droneMinimum.addMandatory(Commands.MOVEMENT_FORWARD_RIGHT);
		droneMinimum.addMandatory(Commands.MOVEMENT_LEFT);
		droneMinimum.addMandatory(Commands.MOVEMENT_RIGHT);
		droneMinimum.addMandatory(Commands.MOVEMENT_START_STOP);

		arDroneProfile.addOutput("Accelerometer", Commands.SENSOR_ACC);
		arDroneProfile.addOutput("Pitch Yaw Roll", Commands.SENSOR_PYR);
		arDroneProfile.addOutput("Gyroscope", Commands.SENSOR_GYRO);

		ambientMediaProfile.addOptionalControls(Commands.DISPLAY_IMAGE);
		ambientMediaProfile.addOptionalControls(Commands.DISPLAY_VIDEO);
		ambientMediaProfile
				.addOptionalControls(Commands.PLAYBACK_BACKWARD_SEEK);
		ambientMediaProfile.addOptionalControls(Commands.PLAYBACK_FORWARD_SEEK);
		ambientMediaProfile.addOptionalControls(Commands.PLAYBACK_NEXT);
		ambientMediaProfile.addOptionalControls(Commands.PLAYBACK_PREVIOUS);

		ControlProfile.ControllableProfileItem mediaminimum = ambientMediaProfile
				.createProfileforPriority(1);
		mediaminimum.addMandatory(Commands.PLAYBACK_PLAY_PAUSE);

		pitchControllerProfile.addOutput("Pitch", Commands.SENSOR_SINGLE_VALUE);
		pitchControllerProfile.addOutput("Loudness",
				Commands.SENSOR_SINGLE_VALUE);
		pitchControllerProfile.addOutput("First Derivative",
				Commands.SENSOR_SINGLE_VALUE);
		pitchControllerProfile.addOutput("Second Derivative",
				Commands.SENSOR_SINGLE_VALUE);

		ControlProfile.ControllableProfileItem artnetIdeal = artNetLightControl
				.createProfileforPriority(1);
		artnetIdeal.addMandatory((Commands.DISPLAY_COLOR));

		for (ControlProfile controlProfile : profiles.values()) {
			PluginControlDescription pluginControlDescription = new PluginControlDescription(
					session, controlProfile);
			session.persist(pluginControlDescription);
		}
    }
}