package org.ambientdynamix.contextplugins.entity;

import com.google.gson.Gson;
import org.ambientdynamix.contextplugins.ambientcontrol.ControlProfile;
import org.ambientdynamix.contextplugins.ambientcontrol.XMLConverter;
import org.ambientdynamix.contextplugins.server.RestEndpoint;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.*;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.*;

/**
 * Created by MsMe on 12.08.2014.
 */

@Entity
public class PluginControlDescription implements Serializable {
	public PluginControlDescription() {	}

    public PluginControlDescription(String artifact_id) {
        this.artifact_id = artifact_id;
    }

    public PluginControlDescription(EntityManager session, ControlProfile controlProfile) {
        this.setName(controlProfile.getName());
        this.artifact_id = (controlProfile.getArtifact_id());
        this.setDescription(controlProfile.getDescription());
        this.setPluginVersion(controlProfile.getPluginVersion());
        // this.setOwner(controlProfile.getOwner_id()); TODO: set Owner
        this.setPlatform(controlProfile.getPlatform());
        this.setMinPlatformVersion(controlProfile.getMinPlatformVersion());
        this.setMinFrameworkVersion(controlProfile.getMinFrameworkVersion());

        for (String command_id : controlProfile.getOptionalInputList()) {
            this.optionalInputList.add(insertCommandItem(session, command_id));
        }
        Map<String, String> map = controlProfile.getOutputList();
        for (String availableControl : map.keySet())
            this.addOutput(new Output(availableControl, insertCommandItem(
                    session, map.get(availableControl))));
        for (ControlProfile.ControllableProfileItem controllableProfileItem : controlProfile
                .getInputList()) {
            Profile profile = new Profile("TODO: CHANGE PROFILE ID"
                    + controllableProfileItem.toString(),
                    controllableProfileItem.getPriority()); // TODO: CHANGE PROFILE ID
            for (String mandatoryControl : controllableProfileItem
                    .getMandatoryControls())
                profile.addCommandItem(insertCommandItem(session,
                        mandatoryControl));
            this.addProfile(profile);
        }
    }

    /**
     * Checks if CommandItem already in DB, if not it will be added
     * @param session
     * @param command_id
     * @return
     */
	private CommandItem insertCommandItem(EntityManager session, String command_id) {
		CommandItem commandItem = null;
		Query query = session
				.createQuery("Select ci from CommandItem ci where ci.command_id=:command_id");
		query.setParameter("command_id", command_id);

		@SuppressWarnings("unchecked")
		List<CommandItem> commandItemList = query.getResultList();

		if (commandItemList.size() <= 0) {
			// System.out.println("command item not in database");
			commandItem = new CommandItem(command_id);
			session.persist(commandItem);
		} else {
			// System.out.println("command item already in database!");
			for (Object obj : commandItemList) {
				commandItem = (CommandItem) obj;
			}
		}
		return commandItem;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", updatable = false, nullable = false, unique = true)
	private Integer id = null;

	@Column(name = "ARTIFACT_ID", updatable = false, nullable = false, unique = true)
	private String artifact_id = null;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "OWNER_ID", unique = false)
	private Owner owner;

	@Column(name = "NAME", nullable = false)
	private String name = null;

	@Column(name = "DESCRIPTION", nullable = false)
	private String description = null;

	@Column(name = "PLUGINVERSION", nullable = false)
	private String pluginVersion = null;

	@Column(name = "PLATFORM", nullable = false)
	private String platform = null;

	@Column(name = "MINPLATFORMVERSION", nullable = false)
	private String minPlatformVersion = null;

	@Column(name = "MINFRAMEWORKVERSION", nullable = false)
	private String minFrameworkVersion = null;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "PluginControlDescription_has_Output", joinColumns = @JoinColumn(name = "ARTIFACT_ID"), inverseJoinColumns = @JoinColumn(name = "ID"))
	private Set<Output> outputList = new HashSet<Output>();

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "PluginControlDescription_has_OptionalInputs", joinColumns = @JoinColumn(name = "ARTIFACT_ID"), inverseJoinColumns = @JoinColumn(name = "COMMAND_ID"))
	private Set<CommandItem> optionalInputList = new HashSet<CommandItem>();

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "PluginControlDescription_has_InputProfiles", joinColumns = @JoinColumn(name = "ARTIFACT_ID"), inverseJoinColumns = @JoinColumn(name = "ID"))
	private Set<Profile> inputList = new HashSet<Profile>();

	public Integer getId() {
		return id;
	}

	public String getArtifact_id() {
		return artifact_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPluginVersion() {
		return pluginVersion;
	}

	public void setPluginVersion(String pluginVersion) {
		this.pluginVersion = pluginVersion;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getMinPlatformVersion() {
		return minPlatformVersion;
	}

	public void setMinPlatformVersion(String minPlatformVersion) {
		this.minPlatformVersion = minPlatformVersion;
	}

	public String getMinFrameworkVersion() {
		return minFrameworkVersion;
	}

	public void setMinFrameworkVersion(String minFrameworkVersion) {
		this.minFrameworkVersion = minFrameworkVersion;
	}

	public Set<Output> getOutputList() {
		return outputList;
	}

	public void setOutputList(Set<Output> outputList) {
		this.outputList = outputList;
	}

	public void addOutput(Output output) {
		Set<Output> set = this.getOutputList();
		set.add(output);
		this.setOutputList(set);
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		// TODO: On change owner always insert PluginControlDescription to the list of the new owner.
		this.owner = owner;
	}

	public Set<CommandItem> getOptionalInputList() {
		return optionalInputList;
	}

	public void setOptionalInputList(Set<CommandItem> optionalInputList) {
		this.optionalInputList = optionalInputList;
	}

	public void addOptionalInput(CommandItem commandItem) {
		Set<CommandItem> set = this.getOptionalInputList();
		set.add(commandItem);
		this.setOptionalInputList(set);
	}

	public Set<Profile> getInputList() {
		return inputList;
	}

	public void setInputList(Set<Profile> inputList) {
		this.inputList = inputList;
	}

	public void addProfile(Profile profile) {
		Set<Profile> set = this.getInputList();
		set.add(profile);
		this.setInputList(set);
	}

    /**
     * Return PluginControlDescription as ControlProfile
     * @return
     */
	public ControlProfile getAsControlProfile() {
		ControlProfile controlProfile = new ControlProfile();
		controlProfile.setName(this.getName());
		controlProfile.setArtifact_id(this.getArtifact_id());
		controlProfile.setDescription(this.getDescription());
		controlProfile.setPluginVersion(this.getPluginVersion());
		// controlProfile.setProvider(this.getOwner().getOwner_id()); TODO: Define Owner
		controlProfile.setPlatform(this.getPlatform());
		controlProfile.setMinPlatformVersion(this.getMinPlatformVersion());
		controlProfile.setMinFrameworkVersion(this.getMinFrameworkVersion());

        if(this.getOptionalInputList() != null) {
            System.out.println("getOptionalInputList" );
            System.out.println("getOptionalInputList "+this.getOptionalInputList());
            for (CommandItem commandItem : this.getOptionalInputList())
                controlProfile.addOptionalControls(commandItem.getCommand_id());
        }
        if(this.getOutputList() != null) {
            for (Output availableControl : this.getOutputList())
                controlProfile.addOutput(availableControl.getName(),
                        availableControl.getCommandItem().getCommand_id());
        }
        if(this.getInputList() != null) {
            for (Profile profile : this.getInputList()) {
                List<String> mandatoryControls = new ArrayList<String>();
                for (CommandItem commandItem : profile.getCommandItems())
                    mandatoryControls.add(commandItem.getCommand_id());
                controlProfile.createProfileforPriority(profile.getPriority(),
                        mandatoryControls);

            }
        }
		return controlProfile;
	}


    public String getDescription(String format) {
        String ret = "";
        if(format != null && format.equals(RestEndpoint.FORMAT_JSON)) {
            ret = this.getDescriptionJSON();
        }else{
            ret = this.getDescriptionXML();
        }
        return ret;
    }
    public String getDescriptionJSON(){
        System.out.println("outputlist size: "+this.getAsControlProfile().getOutputList().size());
        for (String string : this.getAsControlProfile().getOutputList().values())
            System.out.println("outputs string: "+string);
        return formatOutput(this.getAsControlProfile(),RestEndpoint.FORMAT_JSON);
    }
    public String getDescriptionXML(){
        return formatOutput(this.getAsControlProfile(),RestEndpoint.FORMAT_XML);
    }

    /*private ControlProfile runQueryControlProfile(Query query){
        try{
            @SuppressWarnings("unchecked")
            PluginControlDescription pcd = (PluginControlDescription) query.getSingleResult();

            if(pcd == null){
                return null;
            }
            ControlProfile controlProfile = pcd.getAsControlProfile();
            return controlProfile;
        }catch (NoResultException e){

        }
        return null;
    }*/

    private String formatOutput(ControlProfile controlProfile, String format){
        System.out.println("formatOutput");
        System.out.println("CP"+controlProfile);
        String output = "";
        if (controlProfile != null){
            System.out.println("formatOutput: "+controlProfile.getName());
            if(format != null){
                System.out.println(" format: "+ format);
                if (format.equals("html")) {
                    // To Display XML in a HTML Page
                    output = controlProfile.getXMLCode();
                    output = output.replaceAll("<", "&lt;");
                    output = output.replaceAll(">", "&gt;");
                    output = output.replaceAll("\n", "</br>");
                }else if(format.equals("json")){
                    Gson gson = new Gson();                 //TODO: JSONObject stopped to make lists right..
                    output = gson.toJson(controlProfile);
                    //output = new JSONObject(controlProfile).toString();
                    System.out.println("JSON: "+output);
                }else if(format.equals("xml")){
                    output = controlProfile.getXMLCode();
                }
            }else{		//Return XML
                output = controlProfile.getXMLCode();
            }
        }
        return output;
    }
}
