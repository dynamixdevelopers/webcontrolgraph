package org.ambientdynamix.contextplugins.entity;

import org.ambientdynamix.contextplugins.server.RestEndpoint;
import org.json.JSONArray;
import org.json.JSONException;

import javax.persistence.*;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MsMe on 12.08.2014.
 */

@Entity
public class ControlGraph implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", updatable = false, nullable = false, unique = true)
	private Integer id = null;

	@Column(name = "CONTROLGRAPH_ID", updatable = true, nullable = false, unique = false)
	private String controlGraph_id = null;
	
	public ControlGraph(){}
	public ControlGraph(String controlGraph_id){this.controlGraph_id = controlGraph_id;}

	/*@Column(name = "controlGraph_ID", updatable = false, nullable = false, unique = false)
	private String command_id = null;*/

	@Column(name = "CONTROLGRAPH_JSON", unique = false, nullable = true)
	private String controlGraph_json = null;
	
	public String getControlGraph_Json(){
		return controlGraph_json;
	}
	
	public void setControlGraph_Json(String controlGraph_json){
		this.controlGraph_json = controlGraph_json;
	}

	@Column(name = "CONTROLGRAPH_XML", unique = false, nullable = true)
	private String controlGraph_xml = null;
	
	public String getControlGraph_Xml(){
		return controlGraph_xml;
	}
	
	public void setControlGraph_Xml(String controlGraph_xml){
		this.controlGraph_xml = controlGraph_xml;
	}

    public static ControlGraph getCGById(String controlGraph_id){
        Query query = DatabaseMgr.getEntityManager()
                .createQuery("Select cg from ControlGraph cg where cg.controlGraph_id=:controlGraph_id");
        query.setParameter("controlGraph_id", controlGraph_id);
        Object result = null;
        try{
            result = query.getSingleResult();
        }catch (Exception e){
            //entity not in DB
        }
        ControlGraph m = new ControlGraph();
        if(result != null){
            m = (ControlGraph) result;
        }
        DatabaseMgr.close();
        return m;
    }

    public String getById(String controlGraph_id, String format){
        Query query = DatabaseMgr.getEntityManager()
                .createQuery("Select cg from ControlGraph cg where cg.controlGraph_id=:controlGraph_id");
        query.setParameter("controlGraph_id", controlGraph_id);
        Object result = null;
        try{
            result = query.getSingleResult();
        }catch (Exception e){
            //entity not in DB
        }
        ControlGraph m = new ControlGraph();
        if(result != null){
            m = (ControlGraph) result;
        }else{
            //System.out.println("ControlGraph not in DB");
        }
        DatabaseMgr.close();

        if(format != null && format.equals(RestEndpoint.FORMAT_XML)){
            return m.getControlGraph_Xml();
        }else{
            return m.getControlGraph_Json();
        }

    }

    public String getAllControlGraphIDs(){
        Query query = DatabaseMgr.getEntityManager().createQuery(
                "Select cg from ControlGraph cg");
        @SuppressWarnings("unchecked")
        List<ControlGraph> cgList = query.getResultList();
        DatabaseMgr.close();

        ArrayList<String> arrayList = new ArrayList<String>();
        for (ControlGraph cg : cgList) {
            arrayList.add(cg.getControlGraph_id());
        }
        JSONArray jsonArray = new JSONArray(arrayList);

        StringWriter out = new StringWriter();
        try {                       //TODO: necessary?
            jsonArray.write(out);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return out.toString();
    }
    public ControlGraph addControlGraph(String id, String controlGraph, String format){

        //Check if ControlGraph is already available
        EntityManager entityManager = DatabaseMgr.getEntityManager();
        entityManager.getTransaction().begin();
        Query query = entityManager
                .createQuery("Select cg from ControlGraph cg where cg.controlGraph_id=:controlGraph_id");
        query.setParameter("controlGraph_id", id);
        Object result = null;
        try{
            result = query.getSingleResult();
        }catch (Exception e){
            //entity not in DB
        }
        DatabaseMgr.close();

        ControlGraph cg;
        boolean newMatch = true;
        if(result != null){
            cg = (ControlGraph) result;
            newMatch = false;
        }else{
            cg = new ControlGraph(id);
        }
        if(format.equals(RestEndpoint.FORMAT_XML))
            cg.setControlGraph_Xml(controlGraph);
        else if(format.equals(RestEndpoint.FORMAT_JSON))
            cg.setControlGraph_Json(controlGraph);
        entityManager = DatabaseMgr.getEntityManager();
        entityManager.getTransaction().begin();

        if(newMatch)
            entityManager.persist(cg);
        else
            entityManager.merge(cg);
        entityManager.getTransaction().commit();
        DatabaseMgr.close();

        return cg;

    }

    public void delete(){
        EntityManager entityManager = DatabaseMgr.getEntityManager();
        entityManager.getTransaction().begin();

        try{
            ControlGraph controlGraph = entityManager.merge(this);
            entityManager.remove(controlGraph);
        }catch (NoResultException e){
            throw new NoResultException("Can't delete database entity");
        }
        entityManager.getTransaction().commit();
        DatabaseMgr.close();
    }

    public String getControlGraph_id() {
        return controlGraph_id;
    }
}
