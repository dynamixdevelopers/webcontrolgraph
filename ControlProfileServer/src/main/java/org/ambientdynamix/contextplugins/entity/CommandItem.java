package org.ambientdynamix.contextplugins.entity;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by MsMe on 12.08.2014.
 */

@Entity
public class CommandItem implements Serializable {
	public CommandItem() {
	}

	public CommandItem(String command_id) {
		this.command_id = command_id;
	}

	public CommandItem(String command_id, String description) {
		this.command_id = command_id;
		this.description = description;
	}

	public void removePluginControlDescription(PluginControlDescription pcd,
			EntityManager entityManager) {
		optionalInputList.remove(pcd);
		remove(entityManager);
	}

	private boolean isOrphan() {
		if (optionalInputList.size() == 0 && outputControls.size() == 0
				&& inputList.size() == 0  && sourceTranslatorList.size() == 0
                && targetTranslatorList.size() == 0)
			return true;
		return false;
	}

	private void remove(EntityManager entityManager) {
		if (isOrphan()) {
			entityManager.remove(this);
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", updatable = false, nullable = false, unique = true)
	private Integer id = null;

	@Column(name = "COMMAND_ID", updatable = false, nullable = false, unique = false)
	private String command_id = null;

	@Column(name = "DESCRIPTION", unique = false, nullable = true)
	private String description = null;

	public Integer getId() {
		return id;
	}

	public String getCommand_id() {
		return command_id;
	}

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "PluginControlDescription_has_OptionalInputs", joinColumns = @JoinColumn(name = "COMMAND_ID"), inverseJoinColumns = @JoinColumn(name = "ARTIFACT_ID"))
	private Set<PluginControlDescription> optionalInputList = new HashSet<PluginControlDescription>();

	public Set<PluginControlDescription> getOptionalInputList() {
		return optionalInputList;
	}

	public void setOptionalInputList(
			Set<PluginControlDescription> optionalInputList) {
		this.optionalInputList = optionalInputList;
	}

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "Profile_has_CommandItem", joinColumns = @JoinColumn(name = "COMMAND_ID"), inverseJoinColumns = @JoinColumn(name = "PROFILE_ID"))
	private Set<Profile> inputList = new HashSet<Profile>();

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "SourceTranslator_has_CommandItem", joinColumns = @JoinColumn(name = "COMMAND_ID"), inverseJoinColumns = @JoinColumn(name = "SOURCE_COMMANDITEM"))
    private Set<Translator> sourceTranslatorList = new HashSet<Translator>();

    public Set<Translator> getSourceTranslatorList() {
        return sourceTranslatorList;
    }

    public void setSourceTranslatorList(
            Set<Translator> sourceTranslatorList) {
        this.sourceTranslatorList = sourceTranslatorList;
    }

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "TargetTranslator_has_CommandItem", joinColumns = @JoinColumn(name = "COMMAND_ID"), inverseJoinColumns = @JoinColumn(name = "TARGET_COMMANDITEM"))
    private Set<Translator> targetTranslatorList = new HashSet<Translator>();
    public Set<Translator> getTargetTranslatorList() {
        return targetTranslatorList;
    }

    public void setTargetTranslatorList(
            Set<Translator> sourceTranslatorList) {
        this.targetTranslatorList = targetTranslatorList;
    }



	public Set<Profile> getInputList() {
		return inputList;
	}

	public void setProfileList(Set<Profile> inputList) {
		this.inputList = inputList;
	}

	public void getCountInputList() {
		System.out.println("inputList: " + inputList.size());
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "commandItem")
	private Set<Output> outputControls = new HashSet<Output>();

	public Set<Output> getOutputControls() {
		return outputControls;
	}

	public void setOutputControls(Set<Output> outputList) {
		this.outputControls = outputList;
	}

	public void removeOutput(Output output, EntityManager entityManager) {
		outputControls.remove(output);
		remove(entityManager);
	}

	public void removeInput(Profile input, EntityManager entityManager) {
		inputList.remove(input);
		remove(entityManager);
	}

    public void removeSourceTranslator(Translator sourceTranslator, EntityManager entityManager) {
        sourceTranslatorList.remove(sourceTranslator);
        remove(entityManager);
    }
    public void removeTargetTranslator(Translator targetTranslator, EntityManager entityManager) {
        targetTranslatorList.remove(targetTranslator);
        remove(entityManager);
    }

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
