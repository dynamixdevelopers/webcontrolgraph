package org.ambientdynamix.contextplugins.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by MsMe on 12.08.2014.
 */

@Entity
public class Owner {

    public Owner(){}
    public Owner(String owner_id){
        this.owner_id = owner_id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", updatable = false, nullable = false)
    private Integer id = null;

    @Column(name = "OWNER_ID", nullable = false, unique = true)
    private String owner_id = null;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description = null;

    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "owner")
    private Set<PluginControlDescription> pluginControlDescriptions = new HashSet<PluginControlDescription>();



    public Integer getId() {
        return id;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<PluginControlDescription> getPluginControlDescriptions() {
        return pluginControlDescriptions;
    }

    public void setPluginControlDescriptions(Set<PluginControlDescription> pluginControlDescriptions) {
        for( PluginControlDescription pluginControlDescription : pluginControlDescriptions){
            pluginControlDescription.setOwner(this);
        }
        this.pluginControlDescriptions = pluginControlDescriptions;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void addPluginControlDescription(PluginControlDescription pluginControlDescription){
        Set<PluginControlDescription> set = this.getPluginControlDescriptions();
        set.add(pluginControlDescription);
        this.setPluginControlDescriptions(set);
        pluginControlDescription.setOwner(this);
    }

}
