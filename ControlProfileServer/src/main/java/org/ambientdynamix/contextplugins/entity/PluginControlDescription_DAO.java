package org.ambientdynamix.contextplugins.entity;

import com.google.gson.Gson;
import org.ambientdynamix.contextplugins.ambientcontrol.ControlProfile;
import org.ambientdynamix.contextplugins.ambientcontrol.XMLConverter;
import org.ambientdynamix.contextplugins.server.RestEndpoint;
import org.json.JSONArray;
import org.json.JSONException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.io.StringWriter;
import java.util.*;

/**
 * Created by MsMe on 22.09.2014.
 */
public class PluginControlDescription_DAO {


    /**
     * Returns PluginControlDescription from Database if available, else from new PluginControlDescription Object
     * @param artifact_id
     * @return
     */
    public PluginControlDescription getPCDByArtifactID(String artifact_id){
        Query query = DatabaseMgr.getEntityManager().createQuery(
                "Select pcd from PluginControlDescription pcd WHERE pcd.artifact_id = :artifact_id");
        query.setParameter("artifact_id", artifact_id);
        PluginControlDescription pcd = runQueryPluginControlDescription(query);
        System.out.println("getPCDByArtifact_ID");
        System.out.println("Name: "+pcd.getName());
        System.out.println("input-size: "+pcd.getInputList().size());
        return pcd;
    }
    public PluginControlDescription getPCDByName(String name){
        Query query = DatabaseMgr.getEntityManager().createQuery(
                "Select pcd from PluginControlDescription pcd WHERE pcd.name = :name");
        query.setParameter("name", name);
        PluginControlDescription pcd = runQueryPluginControlDescription(query);
        return pcd;
    }
    private PluginControlDescription runQueryPluginControlDescription(Query query){
        PluginControlDescription pcd = null;
        try{
            pcd = (PluginControlDescription) query.getSingleResult();
        }catch (NoResultException e){
            //Not Found in DB
        }
        return pcd;
    }
    /**
     * Return all available Artifact IDs
     * When input/output are not empty, all Objects will be queried to fulfill the request
     * @param valueType requested return type ids|names
     * @param input requested inputs
     * @param output requested outputs
     * @param constraint ONE|ALL
     * @return
     */
    public String getAllArtifactIds(String valueType, String input, String output, String constraint){
        if(output != null || input != null){
            ArrayList<String> inputList =  getArrayListFromString(input);
            ArrayList<String> outputList =  getArrayListFromString(output);

            Set<String> cpSet  = new HashSet<String>();
            if(inputList.size() == 0 && outputList.size() > 0){
                System.out.println("output");
                cpSet = getDescriptionsContainOputputCommands(valueType, outputList, constraint);
            }else if(inputList.size() > 0 && outputList.size() == 0){
                System.out.println("input");
                cpSet = getDescriptionsContainInputCommands(valueType, inputList, constraint);
            }else if(inputList.size() > 0 && outputList.size() > 0){
                cpSet = getDescriptionsContainOputputCommands(valueType, outputList, constraint);
                cpSet.addAll(getDescriptionsContainInputCommands(valueType, outputList, constraint));
            }
            return formatArrayListToJSON(cpSet);
        }
        Query query = DatabaseMgr.getEntityManager().createQuery(
                "Select pcd from PluginControlDescription pcd");
        @SuppressWarnings("unchecked")
        List<PluginControlDescription> pcdList = query.getResultList();
        DatabaseMgr.close();

        String serverReturn = "";
        ArrayList<String> arrayList = new ArrayList<String>();
        if(valueType.equals(RestEndpoint.NAME)){
            for (PluginControlDescription pcd : pcdList) {
                serverReturn = serverReturn + pcd.getName() + "\n";
                arrayList.add(pcd.getName());
            }
        }else{
            for (PluginControlDescription pcd : pcdList) {
                serverReturn = serverReturn + pcd.getArtifact_id() + "\n";
                arrayList.add(pcd.getArtifact_id());
            }
        }
        System.out.println(serverReturn);
        JSONArray jsonArray = new JSONArray(arrayList);

        StringWriter out = new StringWriter();
        try {                       //TODO: necessary?
            System.out.println("Before write JSON");
            jsonArray.write(out);
            System.out.println("After write JSON");
        } catch (JSONException e) {
            System.out.println("ERROR write JSON");
            e.printStackTrace();
        }
        return out.toString();
    }
    private String formatArrayListToJSON(Set<String> cpList){
        String output = "";
        if (cpList != null && cpList.size() > 0){
            //ArrayList always to JSONArray
            JSONArray jsArray = new JSONArray(cpList);
            output = jsArray.toString();

        }
        return output;
    }
    public Set<String> getDescriptionsContainOputputCommands(String valueType, ArrayList<String> outputs, String constraint){
        EntityManager entityManager = DatabaseMgr.getEntityManager();
        Query query;
        System.out.println("outputs.size "+outputs.size());
        if(constraint != null && constraint.equals("ALL")){
            //Contains all outputs
            query = entityManager.createQuery("Select pcd from PluginControlDescription pcd JOIN pcd.outputList oi WHERE oi.commandItem.command_id IN :command_ids group by pcd.artifact_id having count(pcd.artifact_id) = :commandCount ");
            query.setParameter("command_ids", outputs);
            query.setParameter("commandCount", new Long(outputs.size()));
        }else{	//constraint.equals("ONE");
            //Contains at least one output
            query = entityManager.createQuery("Select pcd from PluginControlDescription pcd  JOIN pcd.outputList oi WHERE oi.commandItem.command_id IN :command_ids Group By pcd.artifact_id");
            query.setParameter("command_ids", outputs);
        }

        @SuppressWarnings("unchecked")
        List<PluginControlDescription> availablePluginControlDescriptions = query.getResultList();

        Set<String> returnList = new HashSet<String>();
        for(PluginControlDescription pcd : availablePluginControlDescriptions)
            if(valueType.equals(RestEndpoint.ID))
                returnList.add(pcd.getArtifact_id());
            else
                returnList.add(pcd.getName());
        return returnList;
    }
    public Set<String> getDescriptionsContainInputCommands(String valueType, ArrayList<String> inputs, String constraint){
        EntityManager entityManager = DatabaseMgr.getEntityManager();
        Query query;
        //Contains at least one input, ALL is not supported!
        //query = entityManager.createQuery("Select pcd from PluginControlDescription pcd JOIN pcd.inputList oi JOIN oi.commandItems as ci WHERE ci.command_id IN :command_ids group by oi.id having count(oi.id) = :commandCount ");
        query = entityManager.createQuery("Select pcd from PluginControlDescription pcd JOIN pcd.inputList oi JOIN oi.commandItems as ci JOIN pcd.optionalInputList oil WHERE ci.command_id IN :command_ids OR oil.command_id IN :command_ids group by oi.id");
        query.setParameter("command_ids", inputs);

        @SuppressWarnings("unchecked")
        List<PluginControlDescription> availablePluginControlDescriptions = query.getResultList();

        Set<String> returnList = new HashSet<String>();
        for(PluginControlDescription pcd : availablePluginControlDescriptions)
            if(valueType.equals(RestEndpoint.ID))
                returnList.add(pcd.getArtifact_id());
            else
                returnList.add(pcd.getName());
        return returnList;
    }
    private ArrayList<String> getArrayListFromString(String string){
        ArrayList<String> arrayList = new ArrayList<String>();
        if(string != null && string.length() > 2){
            string =  (String) string.subSequence(1, string.length()-1);
            System.out.println("substring: "+string);
            String[] stringArray = string.split(",");
            arrayList = new ArrayList<String>(Arrays.asList(stringArray));
        }
        return arrayList;
    }


    public PluginControlDescription postPCD(String description,  String format){
        System.out.println("POST");
        System.out.println(description);

        Gson gson = new Gson();
        ControlProfile cp = gson.fromJson(description, ControlProfile.class);


        System.out.println(cp.getArtifact_id());
        return writeToServer(cp.getXMLCode());

    }

    public PluginControlDescription putPCD(String description, String format){
        ControlProfile cp = new ControlProfile();
        if ( format == null || format.equals(RestEndpoint.FORMAT_XML)){ //XML
            cp = new ControlProfile(description);
        }else if(format.equals(RestEndpoint.FORMAT_JSON)){
            Gson gson = new Gson();
            cp = gson.fromJson(description, ControlProfile.class);
        }
        System.out.println(cp.getArtifact_id());
        return writeToServer(cp.getXMLCode());
    }

    /**
     * Try to insert new Profile into Database.
     * Cases:
     *    Update or Insert when: same version, higher version, new in DB
     * 	  Nothing when: Higher version in db
     *
     *    New Plugin: ACTION_INSERT
     *    New Version: ACTION_UPDATE
     *    Outdated Version: ACTION_NOTHING_OUTDATED
     *    Same Version: TABLE_ACTION_NOTHING
     *
     * @param xml ControlProfile in XML-Format
     * @return String of performed action: TABLE_ACTION_...
     */
    public PluginControlDescription writeToServer(String xml){
        EntityManager entiyManager = DatabaseMgr.getEntityManager();
        entiyManager.getTransaction().begin();


        String returnString = "";
        XMLConverter xmlConverter = new XMLConverter();
        ControlProfile controlProfile = xmlConverter.XML2ControlProfile(xml);


        EntityManager entityManager = DatabaseMgr.getEntityManager();
        entityManager.getTransaction().begin();

        Query query = entityManager
                .createQuery("Select pcd from PluginControlDescription pcd where pcd.artifact_id=:artifact_id");
        query.setParameter("artifact_id", controlProfile.getArtifact_id());

        PluginControlDescription availablePluginControlDescription;
        try{
            availablePluginControlDescription = (PluginControlDescription) query.getSingleResult();

            //entity already in DB

            String latestVersion = compareVersion(controlProfile.getPluginVersion(), availablePluginControlDescription.getPluginVersion());
            if(latestVersion.equals(controlProfile.getPluginVersion())){
                //older version on server - insert
                System.out.println(availablePluginControlDescription.getArtifact_id());
                Set<CommandItem> optCont = availablePluginControlDescription.getOptionalInputList();
                optCont.clear();

                //availablePluginControlDescription.setOptionalControlList(new HashSet<CommandItem>());

                entityManager.remove(availablePluginControlDescription);		//TODO: Not the best solution, because it will increase the ids each time

                entityManager.getTransaction().commit();

                entityManager.getTransaction().begin();
                availablePluginControlDescription = new PluginControlDescription(entityManager, controlProfile);
                entityManager.persist(availablePluginControlDescription);
                returnString = RestEndpoint.ACTION_UPDATE;
            }else if (latestVersion.equals(RestEndpoint.ACTION_NOTHING_IDENTICAL)){
                //version already on server - nothing
                returnString = RestEndpoint.ACTION_NOTHING_IDENTICAL;
            }else{
                returnString = RestEndpoint.ACTION_NOTHING_OUTDATED;
            }
        }catch (NoResultException e){
            //entity not in DB
            availablePluginControlDescription = new PluginControlDescription(entityManager, controlProfile);
            entityManager.persist(availablePluginControlDescription);
            returnString = RestEndpoint.ACTION_INSERT;
        }

        entityManager.getTransaction().commit();

        DatabaseMgr.close();
        return availablePluginControlDescription;
    }
    /**
     * Compares two version strings and returns the latest version string.
     *
     * @param str1 Version String1
     * @param str2 Version String2
     * @return String of latest version
     */
    private String compareVersion(String str1, String str2){
        String[] vals1 = str1.split("\\.");
        String[] vals2 = str2.split("\\.");
        int i = 0;
        // set index to first non-equal ordinal or length of shortest version string
        while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i]))
        {
            i++;
        }
        // compare first non-equal ordinal number
        if (i < vals1.length && i < vals2.length)
        {
            if(Integer.valueOf(vals1[i])>Integer.valueOf(vals2[i])){
                return str1;
            }else{
                return str2;
            }
        }
        // the strings are equal or one string is a substring of the other
        // e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
        else
        {
            if(vals1.length == vals2.length){
                return RestEndpoint.ACTION_NOTHING_IDENTICAL;
            }else if (vals1.length > vals2.length){
                return str1;
            } else{
                return str2;
            }
        }
    }

    public void deletePCD(PluginControlDescription pcd){
        EntityManager entityManager = DatabaseMgr.getEntityManager();
        entityManager.getTransaction().begin();

        try{
            PluginControlDescription availablePluginControlDescription = entityManager.merge(pcd);

            Set<CommandItem> set = availablePluginControlDescription.getOptionalInputList();
            for (CommandItem item : set) {
                item.removePluginControlDescription(
                        availablePluginControlDescription, entityManager);
            }
            set.clear();
            pcd.setOptionalInputList(set);

            Set<Output> setOutput = availablePluginControlDescription
                    .getOutputList();
            for (Output output : setOutput) {
                output.remove(availablePluginControlDescription, entityManager);
            }
            setOutput.clear();
            pcd.setOutputList(setOutput);

            Set<Profile> inputs = availablePluginControlDescription
                    .getInputList();
            for (Profile input : inputs) {
                input.remove(availablePluginControlDescription, entityManager);
                input.getCount();
            }
            inputs.clear();
            pcd.setInputList(inputs);

            entityManager.remove(availablePluginControlDescription);
        }catch (NoResultException e){
            //throw new NoResultException("Can't delete database entity");
        }
        entityManager.getTransaction().commit();
        DatabaseMgr.close();
    }
}
