package org.ambientdynamix.contextplugins.entity;

import org.ambientdynamix.contextplugins.server.RestEndpoint;
import org.json.JSONObject;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by MsMe on 12.08.2014.
 */

@Entity
public class Translator implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", updatable = false, nullable = false, unique = true)
    private Integer id = null;

    @Column(name = "TRANSLATOR_ID", updatable = true, nullable = false, unique = true)
    private String translator_id = null;

    public Translator(){}
    public Translator(String translator_id){this.translator_id = translator_id;}

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SOURCE_COMMANDITEM", referencedColumnName = "COMMAND_ID")
    private CommandItem source_commandItem;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "TARGET_COMMANDITEM", referencedColumnName = "COMMAND_ID")
    private CommandItem target_commandItem;

    public Translator(String translator_id,  CommandItem source_commandItem, CommandItem target_commandItem) {
        this.translator_id = translator_id;
        EntityManager entityManager = DatabaseMgr.getEntityManager();
        entityManager.getTransaction().begin();

        setSourceCommandItem(entityManager, source_commandItem);
        setTargetCommandItem(entityManager, target_commandItem);

        entityManager.getTransaction().commit();
        DatabaseMgr.close();
    }

    public CommandItem getSourceCommandItem() {
        return source_commandItem;
    }
    public void setSourceCommandItem(EntityManager session, CommandItem source_commandItem) {
        source_commandItem = insertCommandItem(session, source_commandItem.getCommand_id());     //TODO: Fix with CommandID DAO
        this.source_commandItem = source_commandItem;
    }
    public CommandItem getTargetCommandItem() {
        return target_commandItem;
    }
    public void setTargetCommandItem(EntityManager session, CommandItem target_commandItem) {
        target_commandItem = insertCommandItem(session, target_commandItem.getCommand_id());     //TODO: Fix with CommandID DAO
        this.target_commandItem = target_commandItem;
    }

    public Integer getId() {
        return id;
    }
    public String getTranslatorID(){return translator_id;}
    public void remove(EntityManager entityManager) {
        source_commandItem.removeSourceTranslator(this, entityManager);
        source_commandItem = null;
        target_commandItem.removeTargetTranslator(this, entityManager);
        target_commandItem = null;
        entityManager.remove(this);
    }

    /**
     * Checks if CommandItem already in DB, if not it will be added
     * @param session
     * @param command_id
     * @return
     */
    //TODO: Duplicated.. copied from PluginControlDescription
    private CommandItem insertCommandItem(EntityManager session, String command_id) {
        CommandItem commandItem = null;
        Query query = session
                .createQuery("Select ci from CommandItem ci where ci.command_id=:command_id");
        query.setParameter("command_id", command_id);

        @SuppressWarnings("unchecked")
        List<CommandItem> commandItemList = query.getResultList();

        if (commandItemList.size() <= 0) {
            // System.out.println("command item not in database");
            commandItem = new CommandItem(command_id);
            session.persist(commandItem);
        } else {
            // System.out.println("command item already in database!");
            for (Object obj : commandItemList) {
                commandItem = (CommandItem) obj;
            }
        }
        return commandItem;
    }

    public String getJSONFormat(){
        HashMap<String,String> map = new HashMap<String, String>();
        map.put("translator_id",this.translator_id);
        map.put("source_commandItem",this.source_commandItem.getCommand_id());
        map.put("target_commandItem",this.target_commandItem.getCommand_id());
        return new JSONObject(map).toString();
    }

}
