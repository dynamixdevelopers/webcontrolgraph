package org.ambientdynamix.contextplugins.entity;

import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
public class Profile {

	public Profile() {
	}

	public Profile(String profile_id, int priority) {
		this.priority = priority;
		this.profile_id = profile_id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", updatable = false, nullable = false)
	private Integer id = null;

	@Column(name = "PROFILE_ID", nullable = false, unique = true)
	private String profile_id = null;

	@Column(name = "PRIORITIY", nullable = false)
	private Integer priority = null;

	// List of AvailableControls
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "Profile_has_CommandItem", joinColumns = @JoinColumn(name = "PROFILE_ID"), inverseJoinColumns = @JoinColumn(name = "COMMAND_ID"))
	private Set<CommandItem> commandItems = new HashSet<CommandItem>();

	public void getCount() {
		for (CommandItem commandItem : commandItems)
			commandItem.getCountInputList();
	}

	public Integer getId() {
		return id;
	}

	public String getProfile_id() {
		return profile_id;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Set<CommandItem> getCommandItems() {
		return commandItems;
	}

	public void setCommandItems(Set<CommandItem> controllableProfiles) {
		this.commandItems = controllableProfiles;
	}

	public void addCommandItem(CommandItem commandItem) {
		Set<CommandItem> set = this.getCommandItems();
		set.add(commandItem);
		this.setCommandItems(set);
	}

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "PluginControlDescription_has_InputProfiles", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "ARTIFACT_ID"))
	private Set<PluginControlDescription> pluginControlDescriptions = new HashSet<PluginControlDescription>();

	public Set<PluginControlDescription> getPluginControlProfileDescriptions() {
		return pluginControlDescriptions;
	}

	public void setPluginControlProfileDescriptions(
			Set<PluginControlDescription> pluginControlDescriptions) {
		this.pluginControlDescriptions = pluginControlDescriptions;
	}

	public void remove(PluginControlDescription pluginControlDescription,
			EntityManager entityManager) {
		System.out.println("size:: " + pluginControlDescriptions.size());
		pluginControlDescriptions.remove(pluginControlDescription);
		if (pluginControlDescriptions.size() == 0) {
			for (CommandItem commandItem : commandItems) {
				commandItem.removeInput(this, entityManager);
			}
			commandItems.clear();

            entityManager.remove(this);
		}
	}

}
