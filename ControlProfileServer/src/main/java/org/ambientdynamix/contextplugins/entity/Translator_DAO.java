package org.ambientdynamix.contextplugins.entity;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.ambientdynamix.contextplugins.ambientcontrol.ControlProfile;
import org.ambientdynamix.contextplugins.ambientcontrol.XMLConverter;
import org.ambientdynamix.contextplugins.server.RestEndpoint;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.io.StringWriter;
import java.util.*;

/**
 * Created by MsMe on 22.09.2014.
 */
public class Translator_DAO {


    /**
     * Returns PluginControlDescription from Database if available, else from new PluginControlDescription Object
     * @param translator_id
     * @return
     */
    public Translator getTranslaor(String translator_id){

        Query query = DatabaseMgr.getEntityManager().createQuery(
                "Select tr from Translator tr WHERE tr.translator_id = :translator_id");
        query.setParameter("translator_id", translator_id);
        Translator translator = runQueryTranslator(query);
        return translator;
    }

    private Translator runQueryTranslator(Query query){
        Translator translator = null;
        try{
            translator = (Translator) query.getSingleResult();
        }catch (NoResultException e){
            //Not Found in DB
        }
        return translator;
    }

    public Set<String> getTranslatorIDsQuerySource(String sourceCommand){
        EntityManager entityManager = DatabaseMgr.getEntityManager();
        Query query;
        //Contains all outputs
        query = entityManager.createQuery("Select tr from Translator tr WHERE tr.source_commandItem = :source_commandItem ");
        query.setParameter("source_commandItem", sourceCommand);

        @SuppressWarnings("unchecked")
        List<Translator> translators = query.getResultList();

        Set<String> returnList = new HashSet<String>();
        for(Translator tr : translators)
            returnList.add(tr.getTranslatorID());
        return returnList;
    }

    public String getAllTranslatorIds(String sourceCommand, String targetCommand){
        if(sourceCommand != null || targetCommand != null){

            Set<String> cpSet  = new HashSet<String>();

            EntityManager entityManager = DatabaseMgr.getEntityManager();
            Query query;

            if(sourceCommand != null && targetCommand == null ) {
                query = entityManager.createQuery("Select tr from Translator tr WHERE tr.source_commandItem.command_id = :source_commandItem ");
                query.setParameter("source_commandItem", sourceCommand);
            }else if(sourceCommand == null && targetCommand != null) {
                query = entityManager.createQuery("Select tr from Translator tr WHERE tr.target_commandItem.command_id = :target_commandItem ");
                query.setParameter("target_commandItem", targetCommand);
            }else {
                query = entityManager.createQuery("Select tr from Translator tr WHERE tr.target_commandItem.command_id = :target_commandItem AND tr.source_commandItem.command_id = :source_commandItem ");
                query.setParameter("target_commandItem", targetCommand);
                query.setParameter("source_commandItem", sourceCommand);
            }
            @SuppressWarnings("unchecked")
            List<Translator> translators = query.getResultList();
            System.out.println("translators-size: "+translators.size());

            Set<String> returnList = new HashSet<String>();
            for(Translator tr : translators)
                returnList.add(tr.getTranslatorID());

            String output = "";
            if (returnList != null && returnList.size() > 0){
                //ArrayList always to JSONArray
                JSONArray jsArray = new JSONArray(returnList);
                output = jsArray.toString();
            }

            return output;
        }


        Query query = DatabaseMgr.getEntityManager().createQuery(
                "Select tr from Translator tr");
        @SuppressWarnings("unchecked")
        List<Translator> translators = query.getResultList();
        DatabaseMgr.close();

        String serverReturn = "";
        ArrayList<String> arrayList = new ArrayList<String>();
        for (Translator tr : translators) {
            serverReturn = serverReturn + tr.getTranslatorID() + "\n";
            arrayList.add(tr.getTranslatorID());
        }
        System.out.println(serverReturn);
        JSONArray jsonArray = new JSONArray(arrayList);

        StringWriter out = new StringWriter();
        try {                       //TODO: necessary?
            jsonArray.write(out);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return out.toString();
    }

    public Translator postTranslator(Translator translator){
        System.out.println("POST");
        System.out.println(translator.getTranslatorID());

        return writeToServer(translator);

    }

    public Translator putTranslator(Translator translator){
        return postTranslator(translator);
    }

    /**
     * Try to insert new Profile into Database.
     * Cases:
     *    Update or Insert when: same version, higher version, new in DB
     * 	  Nothing when: Higher version in db
     *
     *    New Plugin: ACTION_INSERT
     *    New Version: ACTION_UPDATE
     *    Outdated Version: ACTION_NOTHING_OUTDATED
     *    Same Version: TABLE_ACTION_NOTHING
     *
     * @param translator ControlProfile in XML-Format
     * @return String of performed action: TABLE_ACTION_...
     */
    public Translator writeToServer(Translator translator){
        EntityManager entityManager = DatabaseMgr.getEntityManager();
        entityManager.getTransaction().begin();

        Query query = entityManager
                .createQuery("Select tr from Translator tr where tr.translator_id=:translator_id");
        query.setParameter("translator_id", translator.getTranslatorID());

        Translator availableTranslator;
        try{
            availableTranslator = (Translator) query.getSingleResult();

            //entity already in DB
            //insert - override
            availableTranslator.remove(entityManager);

            //entityManager.remove(availableTranslator);		//TODO: Not the best solution, because it will increase the ids each time
            entityManager.getTransaction().commit();
            entityManager.getTransaction().begin();
        }catch (NoResultException e){
            //entity not in DB
        }
        Translator translator1 = new Translator(translator.getTranslatorID());
        CommandItem ci1 = translator.getSourceCommandItem();
        translator1.setSourceCommandItem(entityManager, ci1);

        CommandItem ci2 = translator.getTargetCommandItem();
        translator1.setTargetCommandItem(entityManager, ci2);

        System.out.println("translator_id: "+translator1.getTranslatorID());
        entityManager.persist(translator1);
        //translator = entityManager.merge(translator);
        entityManager.getTransaction().commit();

        DatabaseMgr.close();
        return translator;
    }
    /**
     * Compares two version strings and returns the latest version string.
     *
     * @param str1 Version String1
     * @param str2 Version String2
     * @return String of latest version
     */
    private String compareVersion(String str1, String str2){
        String[] vals1 = str1.split("\\.");
        String[] vals2 = str2.split("\\.");
        int i = 0;
        // set index to first non-equal ordinal or length of shortest version string
        while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i]))
        {
            i++;
        }
        // compare first non-equal ordinal number
        if (i < vals1.length && i < vals2.length)
        {
            if(Integer.valueOf(vals1[i])>Integer.valueOf(vals2[i])){
                return str1;
            }else{
                return str2;
            }
        }
        // the strings are equal or one string is a substring of the other
        // e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
        else
        {
            if(vals1.length == vals2.length){
                return RestEndpoint.ACTION_NOTHING_IDENTICAL;
            }else if (vals1.length > vals2.length){
                return str1;
            } else{
                return str2;
            }
        }
    }

    public void delete(Translator translator){
        EntityManager entityManager = DatabaseMgr.getEntityManager();
        entityManager.getTransaction().begin();

        try{
            Translator translator1 = entityManager.merge(translator);
            translator1.remove(entityManager);
        }catch (NoResultException e){
            throw new NoResultException("Can't delete database entity");
        }
        entityManager.getTransaction().commit();
        DatabaseMgr.close();
    }

    public Translator putTranslatorFromJSON(String description) {
        EntityManager entityManager = DatabaseMgr.getEntityManager();
        entityManager.getTransaction().begin();


        //TODO: Parse into Object
        Translator translator = new Translator();
        String translator_id = "";
        String source_commandItem = "";
        String target_commandItem = "";

        try {
            JsonElement root = new JsonParser().parse(description);

            translator_id = root.getAsJsonObject().get("translator_id").getAsString();
            System.out.println("translator_id=" + translator_id);

            source_commandItem = root.getAsJsonObject().get("source_commandItem").getAsString();
            System.out.println("source_commandItem=" + source_commandItem);

            target_commandItem = root.getAsJsonObject().get("target_commandItem").getAsString();
            System.out.println("target_commandItem=" + target_commandItem);

            //TODO: Remove EntityManager, because CommandItems are checked more times then necessary.
            translator = new Translator(translator_id);
            translator.setSourceCommandItem(entityManager, new CommandItem(source_commandItem));
            translator.setTargetCommandItem(entityManager, new CommandItem(target_commandItem));
            entityManager.getTransaction().commit();
            DatabaseMgr.close();
            writeToServer(translator);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  translator;
    }
}
