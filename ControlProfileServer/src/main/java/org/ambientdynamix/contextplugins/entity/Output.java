package org.ambientdynamix.contextplugins.entity;

import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by MsMe on 12.08.2014.
 */

@Entity
public class Output {

    public Output(){}
	public Output(String name, CommandItem commandItem) {
		this.name = name;
		this.commandItem = commandItem;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", updatable = false, nullable = false)
	private Integer id = null;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "COMMAND_ID", referencedColumnName = "COMMAND_ID")
	private CommandItem commandItem;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "PluginControlDescription_has_Output", joinColumns = @JoinColumn(name = "ID"), inverseJoinColumns = @JoinColumn(name = "ARTIFACT_ID"))
	private Set<PluginControlDescription> outputList = new HashSet<PluginControlDescription>();

	public Set<PluginControlDescription> getOptionalInputList() {
		return outputList;
	}

	public void setOptionalInputList(Set<PluginControlDescription> outputList) {
		this.outputList = outputList;
	}

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CommandItem getCommandItem() {
		return commandItem;
	}

	public void setCommandItem(CommandItem commandItem) {
		this.commandItem = commandItem;
	}

	public Integer getId() {
		return id;
	}

	public void remove(PluginControlDescription pluginControlDescription,
			EntityManager entityManager) {
		outputList.remove(pluginControlDescription);
		if (outputList.size() == 0) {
			commandItem.removeOutput(this, entityManager);
			commandItem = null;
            entityManager.remove(this);
		}
	}
}
